// hexgame.h

#ifndef HEXGAME_H
#define HEXGAME_H

#include <algorithm>
#include <ctime>
#include <iostream>
#include "ugraph.h"

class HexGame {

 public:

  // Enumerator classes for board size and user color
  enum class BoardSize : int { TINY= 3, SMALL = 5, MEDIUM=7, LARGE = 11 }; 
  enum class UserColor : int { BLUE = 1, RED = 2 };
  
  // Constructor
  HexGame(BoardSize board_size=BoardSize::SMALL, UserColor user_color=UserColor::BLUE);

  // Draw board using ASCII symbols
  void Draw();

  // Determine if a move is legal
  bool IsMoveLegal(int row, int col, bool verbose);

  // Make a move on the board
  void MakeMove(UserColor user_color, int row, int col);

  // Undo a move, to be used in simulating
  void UndoMove(int row, int col);

  // Get the other user color
  UserColor GetOtherPlayerColor(UserColor user_color);

  // Simulate moves and return the number of wins
  int SimNumWins(int row, int col, UserColor user_color);

  // Player make a move
  void PlayerMakeMove(UserColor user_color);

  // Computer make a move
  void ComputerMakeMove(UserColor user_color);

  // Check if given player won the game
  bool CheckPlayerWon(UserColor user_color);

  // Run game
  static int Play();

 private:

  // Return node id number from board coordinates
  int Convert2NodeNumber(int row, int col);

  // Convert UserColor value to double value
  double ConvertUser2Double(UserColor ucolor);

  // Check to see if node's value is set to a color
  double IsNodeSet2Color(int row, int col, UserColor ucolor);

  // Size of board
  int board_size_;
  
  // Player's color
  UserColor user_color_;
  
  // Graph representation of the hex board
  UndirectedGraph graph_;
};

#endif
