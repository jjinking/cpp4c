// hexgame.cc

#include "hexgame.h"

// Class to contain the number of wins for a move at a given row and column
class PossibleMoveWins {
public:
  int row,col;
  int num_wins;
  PossibleMoveWins(int row, int col, int num_wins): row(row), col(col), num_wins(num_wins) {}
};

// Class to enable comparing of second element in std::pair objects
// Used for reversing the order in priority queue
class PossibleMoveWinsCompare {
public:
  bool operator() (const PossibleMoveWins &pmw1, const PossibleMoveWins &pmw2) const {
    return  pmw1.num_wins < pmw2.num_wins;
  }
};

// Constructor
HexGame::HexGame(BoardSize board_size, UserColor user_color)
  :user_color_(user_color) {
  board_size_ = static_cast<std::underlying_type<BoardSize>::type>(board_size);

  // Initialize board as a graph with default initial node values of -1
  graph_ = UndirectedGraph(board_size_ * board_size_, -1);
}

// Draw board using ASCII symbols
void HexGame::Draw() {
  std::cout << std::endl << std::endl;
  std::cout << "Current board" << std::endl;
  std::cout << "Top left position: (1,1)" << std::endl;
  std::cout << "Bottom right position: (" 
	    << board_size_ << ","
	    << board_size_ << ")" << std::endl;
  std::cout << "X indicates Blue, O indicates Red" << std::endl;
  std::cout << "Blue goes East to West, Red goes North to South" << std::endl << std::endl;

  // Outer loop iterates over rows
  for (int i = 0; i < board_size_; ++i) {

    // Output vertically connecting lines
    for (int a = 0; a < i-1; ++a)
      std::cout << "  ";
    if (i > 0) {
      for (int j = 0; j < board_size_; ++j) {
	std::cout << " \\";
	if (j < board_size_ - 1)
	  std::cout << " /";
      }
      std::cout << std::endl;
    }

    // Output each row
    for (int a = 0; a < i; ++a)
      std::cout << "  ";
    for (int j = 0; j < board_size_; ++j) {
      // Output X for blue nodes
      if (IsNodeSet2Color(i+1, j+1, UserColor::BLUE))
	std::cout << "X";
      else if (IsNodeSet2Color(i+1, j+1, UserColor::RED))
	std::cout << "O";
      else
	std::cout << ".";
      if (j < board_size_ - 1)
	std::cout << " - ";
    }
    std::cout << std::endl;
  }
}

// Determine if a move is legal
bool HexGame::IsMoveLegal(int row, int col, bool verbose=false ) {
  // If move position is not within the board, return false
  if (row < 1 || row > board_size_ || col < 1 || col > board_size_) {
    if (verbose)
      std::cout << "Move is not allowed. Please input row and column values between 1 and " << board_size_ << ", inclusive." << std::endl;
    return false;
  }
  // If position is already taken, return false
  else if (graph_.GetNodeValue(Convert2NodeNumber(row,col)) != -1) {
    if (verbose)
      std::cout << "Move is not allowed. Please select an empty position." << std::endl;
    return false;
  }
  return true;
}

// Make a move on the board
void HexGame::MakeMove(UserColor user_color, int row, int col) {
  // Set the node value to the user's color
  int current_node = Convert2NodeNumber(row, col);
  double user_node_val = ConvertUser2Double(user_color);
  graph_.SetNodeValue(current_node, user_node_val);

  // Set edge values between current node and adjacent nodes with
  // the same user color values to 1
  // Right
  if (col < board_size_ && IsNodeSet2Color(row, col+1, user_color)) {
    int rightnode = Convert2NodeNumber(row,col+1);
    graph_.AddEdge(current_node, rightnode, 1);
  }
  // Left
  if (col > 1 && IsNodeSet2Color(row, col-1, user_color)) {
    int leftnode = Convert2NodeNumber(row,col-1);
    graph_.AddEdge(current_node, leftnode, 1);
  }
  // Above
  if (row > 1 && IsNodeSet2Color(row-1, col, user_color)) {
    int abovenode = Convert2NodeNumber(row-1,col);
    graph_.AddEdge(current_node, abovenode, 1);
  }
  // Bottom
  if (row < board_size_ && IsNodeSet2Color(row+1, col, user_color)) {
    int belownode = Convert2NodeNumber(row+1,col);
    graph_.AddEdge(current_node, belownode, 1);
  }
  // Above Right
  if (col < board_size_ && row > 1 && IsNodeSet2Color(row-1, col+1, user_color)) {
    int aboverightnode = Convert2NodeNumber(row-1, col+1);
    graph_.AddEdge(current_node, aboverightnode, 1);
  }
  // Below Left
  if (col > 1 && row < board_size_ && IsNodeSet2Color(row+1, col-1, user_color)) {
    int belowleftnode = Convert2NodeNumber(row+1, col-1);
    graph_.AddEdge(current_node, belowleftnode, 1);
  }
}

// Undo a move, to be used in simulation
void HexGame::UndoMove(int row, int col) {
  
  // Set the node value to the default value of -1
  int current_node = Convert2NodeNumber(row, col);
  graph_.SetNodeValue(current_node, -1);
 
  // Remove edges between current node and all adjacent nodes
  // Right
  if (col < board_size_) {
    int rightnode = Convert2NodeNumber(row,col+1);
    graph_.DeleteEdge(current_node, rightnode);
  }
  // Left
  if (col > 1) {
    int leftnode = Convert2NodeNumber(row,col-1);
    graph_.DeleteEdge(current_node, leftnode);
  }
  // Above
  if (row > 1) {
    int abovenode = Convert2NodeNumber(row-1,col);
    graph_.DeleteEdge(current_node, abovenode);
  }
  // Bottom
  if (row < board_size_) {
    int belownode = Convert2NodeNumber(row+1,col);
    graph_.DeleteEdge(current_node, belownode);
  }
  // Above Right
  if (col < board_size_ && row > 1) {
    int aboverightnode = Convert2NodeNumber(row-1, col+1);
    graph_.DeleteEdge(current_node, aboverightnode);
  }
  // Below Left
  if (col > 1 && row < board_size_) {
    int belowleftnode = Convert2NodeNumber(row+1, col-1);
    graph_.DeleteEdge(current_node, belowleftnode);
  }
}

// Get the other user color
HexGame::UserColor HexGame::GetOtherPlayerColor(UserColor user_color) {
  if (user_color == UserColor::BLUE)
    return UserColor::RED;
  return UserColor::BLUE;
}

// Simulate moves and return the number of wins
int HexGame::SimNumWins(int row, int col, UserColor user_color) {
  // Make initial move at the given position
  MakeMove(user_color, row, col);

  // Collect all legal positions on board and put into a vector
  std::vector< std::pair<int,int> > legalpositions;
  int legalrow, legalcol;
  for (int i=0; i < board_size_; ++i)
    for (int j=0; j < board_size_ ; ++j) {
      legalrow = i + 1;
      legalcol = j + 1;
      if (IsMoveLegal(legalrow, legalcol))
	legalpositions.push_back(std::make_pair(legalrow, legalcol));
    }

  // Simulate 1000 games for the rest of the available positions
  int simrow, simcol;
  int num_wins = 0;
  UserColor simulatedturn;
  // Keep track of simulated moves
  std::set< std::pair<int,int> > simulatedmoves;
  for (int i=0; i < 1000; ++i) {
    // Shuffle the number of possible moves
    std::random_shuffle(legalpositions.begin(), legalpositions.end());
    
    // Simulate rest of available positions
    simulatedturn = GetOtherPlayerColor(user_color);
    for (auto legalpos: legalpositions) {
      simrow = legalpos.first;
      simcol = legalpos.second;
      MakeMove(simulatedturn, simrow, simcol);
      simulatedmoves.insert(std::make_pair(simrow, simcol));
      // Switch player color for next move
      simulatedturn = GetOtherPlayerColor(simulatedturn);
    }

    // Check who won the simulated game
    if (CheckPlayerWon(user_color))
      num_wins++;

    // Cleanup
    // Undo all the simulated moves
    for (auto simmove: simulatedmoves) {
      UndoMove(simmove.first, simmove.second);
    }
    // Empty the set of simulated moves
    simulatedmoves.clear();
  }

  // Undo the move at this position
  UndoMove(row, col);

  // Return the number of wins
  return num_wins;
}

// Player Make a move
void HexGame::PlayerMakeMove(UserColor user_color) {
  int row, col;
  bool valid_move = false;
  while (!valid_move) {
    if (user_color == UserColor::BLUE)
      std::cout << "Blue's turn" << std::endl;
    else
      std::cout << "Red's turn" << std::endl;
    std::cout << "Player make move" << std::endl;
    std::cout << "Row number: ";
    std::cin >> row;
    std::cout << "Col number: ";
    std::cin >> col;
    valid_move = IsMoveLegal(row, col, true);
  }
  MakeMove(user_color, row, col);
}

// Computer Make a move
void HexGame::ComputerMakeMove(UserColor user_color) {
  if (user_color == UserColor::BLUE)
    std::cout << "Blue's turn" << std::endl;
  else
    std::cout << "Red's turn" << std::endl;
  std::cout << "Computer is thinking..." << std::endl;

  // Find optimal position among legal positions on the board
  // Keep track of simulated moves and their number of wins
  std::priority_queue<PossibleMoveWins, std::vector<PossibleMoveWins>, PossibleMoveWinsCompare> movesqueue;
  int row, col, num_wins;
  
  // Store positions to consider
  std::set< std::pair<int,int> > positions2consider;

  // Consider only the positions surrounding the nodes that have been played so far
  int num_positions_played = 0;
  for (int i = 0; i < board_size_; ++i)
    for (int j = 0; j < board_size_; ++j) {
      row = i + 1;
      col = j + 1;
      // If a position has been played, then consider all the positions surrounding the position
      if (graph_.GetNodeValue(Convert2NodeNumber(row, col)) != -1) {
	num_positions_played ++;
	positions2consider.insert(std::make_pair(row-1, col));
	positions2consider.insert(std::make_pair(row-1, col+1));
	positions2consider.insert(std::make_pair(row, col-1));
	positions2consider.insert(std::make_pair(row, col+1));
	positions2consider.insert(std::make_pair(row+1, col));
	positions2consider.insert(std::make_pair(row+1, col-1));
      }
    }

  // If no play has been made, then put it in the middle
  if (num_positions_played == 0) {
    int middle = board_size_ / 2 + 1;
    positions2consider.insert(std::make_pair(middle, middle));
  }
  
  // If still the number of positions to consider is 0, then consider all possible positions
  if (positions2consider.empty())
    for (int i = 0; i < board_size_; ++i)
      for (int j = 0; j < board_size_ ; ++j)
	positions2consider.insert(std::make_pair(i+1, j+1));

  // Simulate games for the given positions to consider
  for (auto pos2consider: positions2consider) {
    row = pos2consider.first;
    col = pos2consider.second;
    if (IsMoveLegal(row, col)) {
      // Make simulated move at the legal position
      num_wins = SimNumWins(row, col, user_color);
      PossibleMoveWins legalmove(row, col, num_wins);
      movesqueue.push(legalmove);
    }
  }

  // Select the best move
  PossibleMoveWins pmwbest = movesqueue.top();
  std::cout << "Computer moves to ("
	    << pmwbest.row << "," << pmwbest.col << ")" << std::endl;
  MakeMove(user_color, pmwbest.row, pmwbest.col);
}

// Check if given player won the game
bool HexGame::CheckPlayerWon(UserColor user_color) {
  // Check if blue player won
  int start_node, end_node;
  double user_node_val = ConvertUser2Double(user_color);
  if (user_color == UserColor::BLUE) {
    // Check if any of the nodes in the first column is connected to any of the nodes in the last column
    for (int rowstart=1; rowstart <=board_size_; ++rowstart) {
      start_node = Convert2NodeNumber(rowstart, 1);
      if (graph_.GetNodeValue(start_node) == user_node_val)
	for (int rowend=1; rowend <= board_size_; ++rowend) {
	  end_node = Convert2NodeNumber(rowend, board_size_);
	  if (graph_.IsConnected(start_node, end_node))
	    return true;
	}
    }
  }
  // Check if red player won
  else {
    for (int colstart=1; colstart <= board_size_; ++colstart) {
      start_node = Convert2NodeNumber(1,colstart);
      if (graph_.GetNodeValue(start_node) == user_node_val)
	for (int colend=1; colend <= board_size_; ++colend) {
	  end_node = Convert2NodeNumber(board_size_, colend);
	  if (graph_.IsConnected(start_node, end_node))
	    return true;
	}
    }
  }
  return false;
}

// Run game on command line
int HexGame::Play() {
  int userchoice;
  BoardSize boardsize;
  UserColor usercolor;
  
  // Output introduction and board size choices to user
  std::cout << "Game of Hex" << std::endl << std::endl;
  
  // Let player select size of board
 selectsize:
  std::cout << "Please select the size of the board" << std::endl;
  std::cout << "\t 3: Board size  3 x 3" << std::endl;
  std::cout << "\t 5: Board size  5 x 5" << std::endl;
  std::cout << "\t 7: Board size  7 x 7" << std::endl;
  std::cout << "\t11: Board size 11 x 11" << std::endl;
  std::cout << "\t 0: Exit" << std::endl;
  std::cin >> userchoice;
  switch (userchoice) {
  case 3:
    boardsize = BoardSize::TINY;
    goto selectcolor;
  case 5:
    boardsize = BoardSize::SMALL;
    goto selectcolor;
  case 7:
    boardsize = BoardSize::MEDIUM;
    goto selectcolor;
  case 11:
    boardsize = BoardSize::LARGE;
    goto selectcolor;
  case 0:
    return 0;
  default:
    std::cout << "Invalid selection: " << userchoice << std::endl;
    goto selectsize;
  }
    
  // Let player select color
 selectcolor:
  std::cout << "Please select your color.  Remember, Blue goes first" << std::endl;
  std::cout << "\t1: Blue" << std::endl;
  std::cout << "\t2: Red"  << std::endl;
  std::cout << "\t0: Exit" << std::endl;
  std::cin >> userchoice;
  switch (userchoice) {
  case 1:
    usercolor = UserColor::BLUE;
    goto gamestart;
  case 2:
    usercolor = UserColor::RED;
    goto gamestart;
  case 0:
    return 0;
  default:
    std::cout << "Invalid selection: " << userchoice << std::endl;
    goto selectcolor;
  }

  // Start game instance
 gamestart:

  // Initialize random number generator
  srand(time(0));

  HexGame *game = new HexGame(boardsize, usercolor);
  game->Draw();

  int num_moves = 1;
  UserColor turn = UserColor::BLUE;
  while (!game->CheckPlayerWon(turn)) { 
    if (num_moves++ % 2 == 1)
      turn = UserColor::BLUE;
    else
      turn = UserColor::RED;

    // If it is the user's turn, then user make move
    if (turn == usercolor)
      game->PlayerMakeMove(turn);
    else
      game->ComputerMakeMove(turn);
    game->Draw();
  }
  
  // Output game winner
  std::cout << "Game over: ";
  if (turn == UserColor::BLUE)
    std::cout << "Blue wins!" << std::endl << std::endl;
  else
    std::cout << "Red wins!" << std::endl << std::endl;
  return 0;
}

// Return node id number from board coordinates
// Assume row and column numbers are inclusively in [1, board_size_]
int HexGame::Convert2NodeNumber(int row, int col) {
  return board_size_ * (row - 1) + (col - 1);
}

// Convert UserColor value to double value
// by static casting underlying int value to double
double HexGame::ConvertUser2Double(UserColor ucolor) {
  return static_cast<double>
    (static_cast<std::underlying_type<UserColor>::type>(ucolor));
}

// Check to see if node's value is set to a color
double HexGame::IsNodeSet2Color(int row, int col, UserColor ucolor) {
  int nodeval = Convert2NodeNumber(row, col);
  double ucolorval = ConvertUser2Double(ucolor);
  return graph_.GetNodeValue(nodeval) == ucolorval;
}
