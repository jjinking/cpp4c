// ugraph.h

#ifndef UNDIRECTEDGRAPH_H
#define UNDIRECTEDGRAPH_H

#include <fstream> 
#include <limits>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>

// Graph class implementation using adjacency matrix
class UndirectedGraph {

 public:
  // Constructor with initialization list
  // Optional parameters for number of nodes and the initial node values
  UndirectedGraph(int num_nodes=0, double init_val=std::numeric_limits<double>::infinity());

  // Constructor that can load a graph from file
  UndirectedGraph(std::ifstream &in, double init_val=std::numeric_limits<double>::infinity()); 

  // Return the number of vertices in the graph
  inline int V() { return n_nodes_; }

  // Return the number of edges in the graph
  inline int E() { return n_edges_; }

  // Tests whether there is an edge from node x to node y
  bool IsAdjacent(int x, int y);

  // Return vector of all y such that there is an edge from node x to node y
  std::vector<int> Neighbors(int x);

  // Add an edge with value v between nodes x,y
  void AddEdge(int x, int y, int v=1);

  // Delete an edge from x to y, if it exists
  void DeleteEdge(int x, int y);

  // Return value associated with node x
  // If node doesn't exist, return -1
  double GetNodeValue(int x);

  // Set value associated with node x to v
  void SetNodeValue(int x, double v);

  // Return value associated with edge (x,y)
  double GetEdgeValue(int x, int y);

  // Set value associated with edge (x,y) to v
  void SetEdgeValue(int x, int y, double v);

  // Add up the edge costs of a graph
  double TotalEdgeCost();

  // Check to see if two nodes are connected
  bool IsConnected(int x, int y);
  
  // Compute MST using Prim's algorithm
  UndirectedGraph *PrimMST();
  
 private:

  // Graph container
  std::map<int, std::map<int, double> > graph_;

  // Map node ids to values
  std::map<int,double> node_val_;

  // Number of nodes
  int n_nodes_;

  // Number of edges
  int n_edges_;

};

#endif
