// HW #4 Implement Hex board

#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>
#include <iostream>
#include <vector>
#include <queue>
#include <iomanip>
#include <string>
     
using namespace std;

typedef int tCost;

const bool debug=false;

const tCost INFINITY= INT_MAX;
const int UNDEFINED_NODE = INT_MAX;

enum cell_state { EMPTY, HAS_X, HAS_O};

class Nodes
{
public:
  Nodes(tCost value=0.0, bool visited=false, int previous = 0) :
    m_edge_cost(), m_value(value), m_visited(visited),
    m_previous(previous), m_state(EMPTY) {}

  tCost get_value() { return m_value; }
  tCost set_value(tCost value) { m_value = value; }

  tCost get_edge_cost(int x) { return m_edge_cost[x]; }
  tCost set_edge_cost(int x, tCost value) { m_edge_cost[x] = value; }

  bool get_visited() { return m_visited; }
  bool set_visited(bool visited) { m_visited = visited; }

  bool get_previous() { return m_previous; }
  bool set_previous(int previous) { m_previous = previous; }

  void add_edge(tCost cost) { m_edge_cost.push_back(cost); }
    
private:
  vector <tCost> m_edge_cost;
  tCost m_value;
  bool m_visited;
  int m_previous;

  cell_state m_state;
};

class Graph
{
public:
  Graph(int nodes);

  // V (G): returns the number of vertices in the graph
  int get_vertices() { return m_num_nodes; }

  // E (G): returns the number of edges in the graph
  int get_edges() { return m_num_edges; }

  // adjacent(G, x, y): tests whether there is an edge from node x to node y.
  //bool isAdjacent(int x, int y);

  // neighbors(G,x):lists all nodes y such that there is an edge from x to y.

  // add (G, x, y): adds to G the edge from x to y, if it is not there.
  void add_edge(int x, int y, tCost cost) {
    m_nodes[x].set_edge_cost(y, cost);
  }

  // delete (G, x, y): removes the edge from x to y, if it is there.
  //void delete_edge(int x, int y);

  // get_node_value (G, x): returns the value associated with the node x.
  tCost get_node_value(int x) {
    return m_nodes[x].get_value();
  }

  // set_node_value(G, x, a): sets the value associated with the node x to a.
  void set_node_value(int x, tCost value) {
    m_nodes[x].set_value(value);
  }

  // get_edge_value(G, x, y): returns value associated to the edge (x,y).
  tCost get_edge_value(int x, int y) {
    return m_nodes[x].get_edge_cost(y);
  }

  // set_edge_value(G,x,y,v): sets value associated to the edge (x,y) to v.
  void set_edge_value(int x, int y, tCost value) {
    m_nodes[x].set_edge_cost(y, value);
  }

  void set_node_visited(int x, bool visited) {
    m_nodes[x].set_visited(visited);
  }

  bool get_node_visited(int x) {
    return m_nodes[x].get_visited();
  }

  void set_node_previous(int x, int previous) {
    m_nodes[x].set_previous(previous);
  }

  int get_node_previous(int x) {
    return m_nodes[x].get_previous();
  }
    
  void print() {
    int field_width = 3 + 2;
    for (int i = 0; i < m_num_nodes; i++) {
      cout << "\nNode " << i << "; value = " << m_nodes[i].get_value() << ":\n";
      for (int j = 0; j < m_num_nodes; j++) {
	tCost cost = m_nodes[i].get_edge_cost(j);
	if (cost != INFINITY)
	  cout << setw(field_width) << cost << " ";
	else
	  cout << setw(field_width) << " XXX ";
      }
      cout << "\n";
    }
    cout << "\n";
  }

private:
  int m_num_nodes;
  int m_num_edges;
  vector <Nodes> m_nodes;
};

Graph::Graph(int nodes) {
  m_num_nodes = nodes;
  Nodes elem;

  for (int i = 0; i < nodes; i++) {
    m_nodes.push_back(elem);
    for (int j = 0; j < nodes; j++)
      m_nodes[i].add_edge(INFINITY);
  }
}

// Priority Element
struct PElem {
  PElem(int x = 0, tCost cost = 0.0): m_x(x), m_cost(cost) {}
  int m_x;
  tCost m_cost;
};

class CompareElem {
public:
  // Returns true if e1 is less than e2
  bool operator()(PElem& e1, PElem& e2)
  {
    if (e1.m_cost < e2.m_cost)
      return true;
    else
      return false;
  }
};


// Dijkstra's algorithm from
// http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
// rewritten in C++
void Dijkstra(Graph &graph, int source) {
  int v;

  // Initializations
  for (v=0; v < graph.get_vertices(); v++) {
    graph.set_node_value(v, INFINITY); // Unknown distance from src to v 
    graph.set_node_visited(v, false); // Nodes have not been visited
    graph.set_node_previous(v, UNDEFINED_NODE); // Previous node in optimal path
  }

  graph.set_node_value(source, 0.0); // Distance from source to source

  priority_queue<PElem, vector<PElem>, CompareElem> pq;
  PElem pq_source = PElem(source, 0.0);
  pq.push(pq_source);         // Start off with just the source node
                                                                
  while (!pq.empty()) {               // The main loop
    // u = vertex in Q with smallest distance in dist[] and
    // has not been visited;  // Source node in first case
    PElem uElem = pq.top();
    pq.pop();

    int u = uElem.m_x;
    graph.set_node_visited(u, true); // mark this node as visited
          
    // for each neighbor v of u
    for (v = 0; v < graph.get_vertices(); v++) {
      // accumulate shortest dist from source
      tCost alt = graph.get_node_value(u) + graph.get_edge_value(u, v);
      if (alt < graph.get_node_value(v) && (graph.get_edge_value(u,v) != INFINITY) && !graph.get_node_visited(v)) {
	// keep the shortest dist from src to v
	graph.set_node_value(v, alt);
	graph.set_node_previous(v, u);

	//insert v into Q; // Add unvisited v into the Q to be process
	PElem pq_v = PElem(v, alt);
	pq.push(pq_v);
      }
    }
  }
  return;
}


class HexBoard {
public:
  HexBoard(int size=11);
  void print_board();
  void print_dist() { m_graphx.print(); m_grapho.print(); }
  cell_state get_cstate(int col, int row) { return m_state[row*m_size + col]; }
  void set_cstate(int col, int row, cell_state state);
  bool validMove(int col, int row);
  bool Player1Winner();
  bool Player2Winner();
  void incrTotalMoves() { m_total_moves++; }
  int getTotalMoves() { return m_total_moves; }
    
private:
  // Helper: Add bidirectional edge if neighbor is also our state
  void add_hex_edge(Graph &graph, cell_state state, int c_col, int c_row, int n_col, int n_row) {
    bool special = false;
    if ((n_col > m_size-1) && (n_col < m_size + 2) && (n_row == m_size-1))
      special = true;
    else if ((n_col < 0) || (n_col >= m_size) || (n_row < 0) || (n_row >= m_size))
      return;

    const int COST = 1;
    int cell = c_row * m_size + c_col;
    int neighbor = n_row * m_size + n_col;

    if (debug)
      cout << "Adding edge " << cell << " to " << neighbor << endl;
    graph.add_edge(cell, neighbor, COST);
    if (special || get_cstate(n_col, n_row) == state) {
      graph.add_edge(neighbor, cell, COST);
      if (debug)
	cout << "Adding edge " << cell << " to " << neighbor << endl;
    }
  }

  int m_size;
  int m_total_moves;
  vector <cell_state> m_state;
  Graph m_graphx;
  Graph m_grapho;
  int m_src_col;
  int m_src_row;
  int m_dest_col;
  int m_dest_row;
};

// The graph representation of the hex board includes 2 virtual nodes for
// start and end points.  There are two graphs (one for x and one for o).
HexBoard::HexBoard(int size): m_size(size), m_total_moves(0), m_state(), m_graphx(size*size+2), m_grapho(size*size+2) {
  for (int i = 0; i < size * size; i++)
    m_state.push_back(EMPTY);
  m_src_col = size;
  m_src_row = size-1;
  m_dest_col = size + 1;
  m_dest_row = size-1;
}

// Create hex graph representation
// Neighbors of B2    If 4x4 and first cell = 0,
//         B1 C1        1 2       0  1  2  3
//      A2 B2 C2      4 5 6       4  5  6  7
//      A3 B3         8 9         8  9 10 11
//                               12 13 14 15
void HexBoard::set_cstate(int col, int row, cell_state state) {
  Graph *gptr = NULL;
  m_state[row*m_size + col] =  state;
  if (state == HAS_X) {
    gptr = &m_graphx;
    // Player 1 has to go West to East (col 0 to col size -1)
    if (col == 0)
      add_hex_edge(*gptr, state, col, row, m_src_col, m_src_row); // Add connection to virtual source
    if (col == m_size-1)
      add_hex_edge(*gptr, state, col, row, m_dest_col, m_dest_row);// Add connection to virtual destination
  }
  else if (state == HAS_O) {
    gptr = &m_grapho;
    // Player 2 has to go North to South (row 0 to row size -1)
    if (row == 0)
      add_hex_edge(*gptr, state, col, row, m_src_col, m_src_row); // Add connection to virtual source
    if (row == m_size-1)
      add_hex_edge(*gptr, state, col, row, m_dest_col, m_dest_row);// Add connection to virtual destination
  }
  else
    return;
  if (col != 0) {
    // Assuming B2, add A2 and A3 as neighbor
    add_hex_edge(*gptr, state, col, row, col-1, row); // A2
    if (row < m_size-1)
      add_hex_edge(*gptr, state, col, row, col-1, row+1); // A3
  }
  if (row != 0) {
    // Add B1 and C1
    add_hex_edge(*gptr, state, col, row, col, row-1); // B1
    if (col < m_size-1)
      add_hex_edge(*gptr, state, col, row, col+1, row-1); // C1
  }
  // Add B3
  if (row < m_size-1)
    add_hex_edge(*gptr, state, col, row, col, row+1);
              
  // Add C2
  if (col < m_size-1)
    add_hex_edge(*gptr, state, col, row, col+1, row);   
}

// print_board()
// Sample board from https://class.coursera.org/cplusplus4c-002/forum/thread?thread_id=676
//
//01234567890123456789012345678901234567890123456789012345678901234567890123456789
//                  NORTH
//  A   B   C   D   E   F   G   H   I   J   K
//1  .   .   .   .   .   .   .   .   .   .   .  1
//  2  .   .   .   .   .   .   .   .   .   .   .  2
//    3  .   .   .   .   .   .   .   .   .   .   .  3
//      4  .   .   .   .   .   .   .   .   .   .   .  4
//        5  .   .   .   .   .   .   .   .   .   .   .  5
//  WEST    6  .   .   .   .   .   .   .   .   .   .   .  6    EAST
//            7  .   .   .   .   .   .   .   .   .   .   .  7
//              8  .   .   .   .   .   .   .   .   .   .   .  8
//                9  .   .   .   .   .   .   .   .   .   .   .  9
//                 10  .   .   .   .   .   .   .   .   .   .   .  10
//                   11  .   .   .   .   .   .   .   .   .   .   .  11
//                        A   B   C   D   E   F   G   H   I   J   K 
//
//                                            SOUTH
void HexBoard::print_board() {
  int col, row;
  cout << "                  NORTH" << endl;
    
  // Output columns title
  for (char col_ch = 'A'; col_ch < 'A' + m_size; col_ch++) {
    cout << "  " << col_ch << " ";
  }
  cout << endl;
    
  for (row = 0; row < m_size; row++) {
    if (row != 5)
      cout << setw(row*2 + 1) << row+1;
    else
      cout << "  WEST    6";

    for (col = 0; col < m_size; col++) {
      cell_state state = get_cstate(col, row);
      switch(state) {
      case EMPTY:
	cout << "  . ";
	break;
      case HAS_X:
	cout << "  x ";
	break;
      case HAS_O:
	cout << "  o ";
	break;
      }
    }
    cout << row+1;
    if (row == 5)
      cout << "    EAST";
    cout << endl;
  }
    
  // Output columns title
  cout << setw(m_size * 2) << " ";
  for (char col_ch = 'A'; col_ch < 'A' + m_size; col_ch++) {
    cout << "  " << col_ch << " ";
  }
  cout << endl;
  cout << "                                            SOUTH" << endl;
}

bool HexBoard::validMove(int col, int row) {
  bool status;
  if ((col < 0) || (col >= m_size) || (row < 0) || (row >= m_size))
    status = false;
  else
    status = get_cstate(col, row) == EMPTY;
  return status;
}

bool HexBoard::Player1Winner() {
  bool status = false;
  Dijkstra(m_graphx, m_src_row*m_size+m_src_col);
  if (m_graphx.get_node_value(m_dest_row*m_size + m_dest_col) != INFINITY)
    status = true;
  return status;
}

bool HexBoard::Player2Winner() {
  bool status = false;
  Dijkstra(m_grapho, m_src_row*m_size+m_src_col);
  if (m_grapho.get_node_value(m_dest_row*m_size + m_dest_col) != INFINITY)
    status = true;
  return status;
}

bool get_move(HexBoard &board, const char *player, int &col, int &row) {
  bool status = true;
  string line;
  cout << player << " enter location: ";
  getline(cin, line);
  if (line.c_str()[0] == '.')
    {
      cout << "'.' entered.  Exiting program" << endl;
      board.print_dist();
      exit(0);
    }
  col = toupper(line.c_str()[0]) - 'A';
  row = atoi(&line.c_str()[1]) - 1;
  status = board.validMove(col, row);

  return status;
}


int main(int argc, char** argv) {
  int size;
  bool done = false;
  int total_moves = 0;
    
  cout << "Enter size one one side of hex board: ";
  cin >> size;
  if ((size > 26) || (size < 2)) {
    cout << "Invalid size " << size << endl;
    exit(EXIT_FAILURE);
  }
  cin.ignore(INT_MAX,'\n');
  cout << endl << "Playing with " << size << "x" << size << " board" << endl;
  cout << "Player 1 is 'x' and should connect West-East" << endl;
  cout << "Player 2 is 'o' and should connect North-South" << endl << endl;
  HexBoard board = HexBoard(size);
  while (!done)
    {
      int row, col;
      cout << "Move " << total_moves << endl;
      board.print_board();
      while (!get_move(board, "Player 1 (x)", col, row))
	cout << "Error" << endl;
      board.incrTotalMoves();
      board.set_cstate(col, row, HAS_X);
      board.print_board();
      if (board.Player1Winner()) {
	cout << "Player 1 has won!" << endl;
	exit(0);
      }
      while (!get_move(board, "Player 2 (o)", col, row))
	cout << "Error" << endl;
      total_moves++;
      board.set_cstate(col, row, HAS_O);
      if (board.Player2Winner()) {
	cout << "Player 2 has won!" << endl;
	exit(0);
      }
    }
}
