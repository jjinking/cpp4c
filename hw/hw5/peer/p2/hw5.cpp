#include <iostream>
#include <cstdlib>
#include <vector> 
#include <string>
#include <sstream> //  manipulating internal string

using namespace std;


//-------------------------------------------------------------------------------------------
// Some Global Constant
//-------------------------------------------------------------------------------------------
int const CostEdge = 1; // because the distance is the same in each hexagone of board
string const abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // player1 coordenates
string const player1 = "GAMER_01"; // in the next homework this will be changed by HUMAN
string const player2 = "GAMER_02"; // in the next homework this will be changed by COMPUTER
//-------------------------------------------------------------------------------------------


enum pieceColor {Blue, Red, White}; // player1(X), player2(O), node not visit(.)

ostream& operator<< (ostream& out, pieceColor c){
  switch (c){
  case Blue: out << player1; break;        // player1(X)
  case Red: out << player2; break;         // player2(O)
  case White: out << "White"; break;       // node not visit(.)
  }
  return out;
}



/*
  ========================================
     class Weighted quick-union
     with path compression (WQUPC)
  ========================================
   Note:
        this class (WQUPC) wasn't
        implementation with class board
        Then only won who put last piece
        on the board 
  ========================================
*/
class WQUPC
{
public:
  WQUPC(pieceColor player, vector< pieceColor > board, vector<int> ori, vector<int> adj):
    gamer(player),id(board),og(ori),ad(adj){QuickUnion();};
  void QuickUnion(){sz.resize(id.size(),0);} // initialize vector "sz"
  int root(int i);
  inline bool find(int p, int q){return root(p)==root(q);}; // check if p and q is in the same root
  void unite(int p, int q);
      
private:
  pieceColor gamer; // analize if player's path is complete 
  vector< pieceColor > id; // give us piece position on the board
  vector<int> sz; // count number of element in each node
  vector<int> og; // node origen
  vector<int> ad; // node adjacent
};


//-------------------------------
// class WQUPC implementation
//-------------------------------
int WQUPC::root(int i)
{
  while (i != id[i])
    {
      id[i] = id[id[i]]; // path compression
      i = id[i]; 
    };
  return i;
}


void WQUPC::unite(int p, int q)
{
  int i=root(p);
  int j=root(q);
  /* 
     
      /// merge smaller tree into larger tree
      /// this part don't work
     
     if(sz[i]<sz[j]){ id[i] = j; sz[j] += sz[i]; }
     else{ id[j] = i; sz[i] += sz[j]; }
     
  */
     
}



//====================================
// class board declaration
//====================================
class Board
{
public:
  Board():_W(7),_H(7){deployBoard();selectMove();}; // default size 7 by 7
  Board(int sizeW, int sizeH):
    _W(sizeW),_H(sizeH){assert(_W>0 && _W==_H&& _H<abc.size());deployBoard();selectMove();} // size N by M, and N == M and less than 27
  void neighbords();
  void deployBoard();
  void selectMove();
  bool checkWinner(pieceColor player);
      
private:
  int _W,_H;
  WQUPC* path1; // check if player1 completed path (not implemented)
  WQUPC* path2; // check if player2 completed path (not implemented)
  vector< pieceColor > nodeVisit; // board matrix
  vector<int> u; // node origen
  vector<int> v; // node adjacent         
};



//-------------------------------
// class board implementation
//-------------------------------
void Board::deployBoard()
{
  // some variables
  string pos, diag, row, headHuman, line, spaceLeft;
  pieceColor p1 = Blue;
  pieceColor p2 = Red;
  pieceColor ep = White; 
     
  // initialization board matrix in list index
  if (nodeVisit.size()==0){
    this->nodeVisit = vector<pieceColor> (_W*_H, ep);
    /*
        for(int k=0; k<nodeVisit.size(); k++){
	cout<<"index "<<k<<" "<<nodeVisit[k]<<endl;}
    */
    neighbords(); // defined neighbords 
        
    //======================================================
    // print keys
    //======================================================
    cout<<"\n  -------------------------------\n";
    cout<<"             HEX GAME\n";
    cout<<"  -------------------------------\n";
    cout<<"       Empty Location:\t"<<".\n";
    cout<<"       "<<player1<<" Piece: \tX\n";
    cout<<"       "<<player2<<" Piece:\tO\n";
    cout<<"  -------------------------------\n";
    cout<<"   Dimension Board: "<<_W<<" x "<< _H<<"\n";
    cout<<"  -------------------------------\n";
    cout<<"   Please!!! 'Caps lock' turn on\n";
    cout<<"  -------------------------------\n";
    cout<<"   C++ for C Programmer\n";
    cout<<"   Homework 4\n";
    cout<<"  -------------------------------\n";
    //======================================================
  }
     
  //print head board
  headHuman=player1;
  line="  ";
  diag="   ";
  for(int g=0; g<player2.size(); g++){spaceLeft+=" ";} // info player two on the table
     
  for(int k=0; k< _H ;k++){
    diag+= abc.substr(k, 1) + " "; //catch letter position of "player1"
    line+="--";
    if(k>1){headHuman.insert(0," ");}
  }
     
  // print information about "player1" on up board
  cout<<"\n\n"<<spaceLeft<<headHuman<<endl;
  cout<<spaceLeft<<line<<endl;
  cout<<spaceLeft<<diag<<endl;
     
  bool control=true; //print one time name of "player2"   
  for(int i = 0; i < _H; i++){
             
    // check piece position
    for(int j = 0; j < _W; j++){
      if (nodeVisit[i*_H+j]==Blue){pos="X";}
      if (nodeVisit[i*_H+j]==Red){pos="O";}
      if (nodeVisit[i*_H+j]==White){pos=".";}
      row += pos + " ";

    }
             
    // config print board body
    string sp=" ";
    if(i>0){for(int g=0;g<i;g++){sp += " ";};}
    double q = _H/(i+1.0);
    string compRight, compLeft;
    compLeft=spaceLeft ;
             
    // check if print name of "player2"
    if(q>=1.5 && q<2.5&&control){
      compRight= "    " + player2; // print information about "player2" on right board
      compLeft=  " " + player2;    // print information about "player2" on left board
      int len = sp.size()+spaceLeft.size(); // adjust print name of "player2"
      if(len>compLeft.size()){sp = ""; for(int g=0;g<len-compLeft.size();g++){sp += " ";};} // setup space left
      control=false;};
             
    cout<<compLeft<<sp<<i+1<<" "<<row<<i+1<<compRight<<endl;
    row = "";
    diag.insert(0," ");
    headHuman.insert(0," ");
    line.insert(0," ");
  }
     
  // print information about "player1" on bottom board
  line.insert(0," ");
  cout<<"\n"<<spaceLeft<<diag <<endl;
  cout<<spaceLeft<<line<<endl;
  cout<<spaceLeft<<headHuman<<"\n\n"<<endl;            
  return;
}


void Board::neighbords()
{
  /*
     
       ========================================
        Explain Algorithm:
       ========================================       
        i = row, j = column
        form vector = i*N + j,
        where N is dimension Board
                    
                    (i,j)
                      u --------- v(i,j+1) 
                     / \          "Case 3"
                    /   \
                   /     \
                  /       \
                 v         v
          (i+1,j-1)        (i+1,j)
          "Case 1"          "Case 2"
              
       ========================================
        Note:
       ========================================
        The graph is undirect so
        It's not necessary repeat information:
        from node "u" to node "v" and
        from node "v" to node "u"     
       ========================================
       
  */
     
  for(int i = 0; i < _H; i++){
    for(int j = 0; j < _W; j++){
            
      // "Case 1"
      if((i+1<_H) && (j-1>=0)){       
	u.push_back(i*_H+j); v.push_back((i+1)*_H+(j-1));
      };
            
      // "Case 2"
      if(i+1<_H){                
	u.push_back(i*_H+j); v.push_back((i+1)*_H+j);
      };
                    
      // "Case 3"
      if(j+1<_W){     
	u.push_back(i*_H+j); v.push_back(i*_H+(j+1));
      };
          
    }
  }
  /*
    cout<<"\n"<<endl;
     for(int k=0; k!=u.size(); k++) //check page 111 C++ For C Programmers 3th Edition
     cout<<u[k]<<" "<<v[k]<<" "<<nodeVisit[u[k]]<<" "<<nodeVisit[v[k]]<<endl;
     cout<<"\n"<<endl;
  */    
  return;
}

void Board::selectMove()
{

  string ans; // ask you if you want to go first
  while (true)
    {
      cout<<" "<<player1<<" (X), do you want to play first (Y or N)? ";
      cin >> ans;
      if (ans=="Y"|| ans=="N"){
	cout<<"\n\n"<<endl;
	break;}
    }
    
  bool game_over = false; // game begin
  bool turn = false; // control turn of game play
    
  // pick positon until than game_over == true
  while(!game_over) 
    {
      // GAMER_01 INPUT
      if(ans=="Y"||turn){while(!game_over)
	  {
            cout<<"\n "<<player1<<" (X) \n";
            cout<<" Please select your position from ";
            cout<<"[A - "<<abc.substr(_W-1, 1)<<"] to [1 - "<<_H<<"] (Notation Together): ";
            string coord;
            int row, column; // coordenates of matrix
            
            cin >> coord;
            
            column = abc.find(coord.substr(0, 1),0); // take first char
            ostringstream oss;
            oss << coord.substr(1); // take second until final characters of string
            istringstream iss(oss.str()); // convert string in integer
            iss >> row;
            --row; // dimension vector start on zero in C++
            
            if((row>=0&&row<=_H)&&(column>=0&&column<=_W)){
	      pieceColor p1 = Blue;
	      if (nodeVisit[row*_H+column]==White){
		nodeVisit[row*_H+column]=p1;
		game_over = checkWinner(p1);
		deployBoard();
		if(game_over){
		  cout<<"\n WON: "<<p1<<"\n CONGRATULATIONS!!!\n\n"<<endl;};
		break;
	      }
	      else{cout<<" position: "<<coord<<" isn't legal please put again your position"<<endl;}}
            
            else{cout<<" position: "<<coord<<" isn't legal please put again your position"<<endl;} 
	  }}
      if(ans=="Y"){turn=true;}   
        
        
      // GAMER_02 INPUT
      if(ans=="N"||turn){while(!game_over)
	  {
            cout<<"\n "<<player2<<" (O) \n";
            cout<<" Please select your position from ";
            cout<<"[A - "<<abc.substr(_W-1, 1)<<"] to [1 - "<<_H<<"] (Notation Together): ";
            
            string coord;
            int row, column; // coordenates of matrix
            cin >> coord;
            
            column = abc.find(coord.substr(0, 1),0); // take first char
            ostringstream oss;
            oss << coord.substr(1); // take second until final characters of string
            istringstream iss(oss.str()); // convert string in integer
            iss >> row;
            --row;  // dimension vector start on zero in C++
            
            if((row>=0 && row<=_H)&&(column>=0 && column<=_W)){
	      pieceColor p2 = Red;
	      if (nodeVisit[row*_H+column]==White){
		nodeVisit[row*_H+column]=p2;
		game_over = checkWinner(p2);
		deployBoard();
		if(game_over){
		  cout<<"\n WON: "<<p2<<"\n GAME OVER MAN\n\n"<<endl;};
		break;
	      }
	      else{cout<<" position: "<<coord<<" isn't legal please put again your position"<<endl;}}
                
            else{cout<<" position: "<<coord<<" isn't legal please put again your position"<<endl;}
	  }}
      if(ans=="N"){turn=true;}                      
    }
}



/*
  ========================================
   check who put last piece on the board
  ========================================
   Note:
        Class (WQUPC) wasn't
        implementation with class board
        Then only won who put last piece
        on the board 
  ========================================
*/
bool Board::checkWinner(pieceColor player)
{
  bool result=true;
  for(int i = 0; i < _H; i++){
    if(!result){break;};
    for(int j = 0; j < _W; j++){
      if (nodeVisit[i*_H+j]==White){result=false;break;}
    }
  }
     
  return result;
            
}
 

 
int main()
{
  //Board myTablet; // default 7 by 7
  Board myTablet(5,5); // 11 by 11 or any dimension

  system("pause");
  return 0;
}
