#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <set>
#include <queue>

using namespace std;
class vertex {
public:
  // Simple constructor for a basic vertex representation
  vertex(int number = 0, double weight = 0.0):vertexNumber(number),weight(weight){}
  // Getter for the vertex key
  double get_number() const {
    return vertexNumber;
  }
  // Setter for the weight
  void set_weight(const double newWeight) {
    weight = newWeight;
  }
  // Getter for the weight
  double get_weight() const {
    return weight;
  }
  // Override the () operator to support class's use as a comparator in a priority queue.
  // Note that it sorts vertices with smallest weight first.
  bool operator() (const vertex& first, const vertex& second) const {
    return (first.get_weight() > second.get_weight());
  }
  // Override the < operator to support use in a set
  bool operator< (const vertex& right) const {
    return vertexNumber > right.get_number();
  }
private:
  int vertexNumber; // The number of the vertex in the graph (its key)
  double weight; // The current weight (cost, distance) of the vertex
};
typedef priority_queue<vertex, vector<vertex>, vertex> vertexPQ;

// Class to represent an undirected graph via a connectivity matrix. Assumes positive edge weights and denotes
// an absent edge by a weight of 0.0 in the matrix.
class graph {
public:
  // Constructor for an empty graph (no edges) with a specified number of vertices.
  // The connectivity matrix is pre-filled with all zeros by the construction list.
  graph(int numVertices) : numVertices(numVertices), connectivityMatrix(numVertices, vector<double> (numVertices, 0.0)) {
  }

  // Method to calculate the shortest path between two vertices using Dijkstra's algorithm
  // where all vertices must be in a passed collection of ids. A return value of zero means no
  // path exists.
  double shortest_path(int start, int end, set<int> allowedVertices) {
    // If the start or end vertices are not in the allowed set, no point continuing
    if ((allowedVertices.find(start) == allowedVertices.end()) ||
	(allowedVertices.find(end) == allowedVertices.end())) {
      return 0.0;
    }

    set<vertex> closed; // The closed set contains the vertices we are finished with
    vertexPQ open; // The open set (queue) contains the vertices we still might need to examine,
    // with the lowest weight vertex first

    // Add the start vertex to the open set with a weight of zero
    // (since it costs nothing to get there)
    vertex current = vertex(start, 0.0);
    open.push(current);

    // Loop through the open set until we get to the end vertex or the open set is empty
    while ((current.get_number() != end) && (open.size() > 0)) {
      // Get the first "open" vertex from the priority queue and remove it from the queue
      // (the first is always the one with least cost)
      current = open.top();
      open.pop();

      // If the new vertex is in the closed set already, nothing needs to be done with it
      if (closed.find(current) != closed.end()) {
	continue;
      }

      // Add the new vertex to the closed set (we have reached it now)
      closed.insert(current);

      // Loop through all possible edges from the new vertex
      for (int i = 0; i < numVertices; ++i) {
	// If the edge weight is non-zero (i.e. the edge exists), process the connected vertex
	// (checking for non-zero also effectively excludes the current vertex from being processed again
	// as we assume no loops)
	double edgeWeight = connectivityMatrix.at(current.get_number()).at(i);
	if (edgeWeight > 0.0) {
	  // Also check that the vertex is an "allowed" one before proceeding
	  if (allowedVertices.find(i) != allowedVertices.end()) {
	    // Calculate the connected vertex's weight as the current vertex's plus the edge cost
	    vertex dest = vertex(i, edgeWeight + current.get_weight());
	    // If the connected vertex is not in the closed set, put it in the open set
	    if (closed.find(dest) == closed.end()) {
	      open.push(dest);
	    }
	  }
	}
      }
    }

    if (current.get_number() == end) {
      return current.get_weight();
    } else {
      return 0.0;
    }
  }

  // Method to add an undirected edge with weight 1.0 between the two vertices
  void add_simple_edge(int sourceVertex, int targetVertex) {
    connectivityMatrix.at(sourceVertex).at(targetVertex) = 1.0;
    connectivityMatrix.at(targetVertex).at(sourceVertex) = 1.0;
  }

protected:
  int numVertices; // Number of vertices in the graph
  vector<vector<double> > connectivityMatrix; // The connectivity matrix represented as a vector of vectors
  // Probably more efficient to use a single vector with an offset,
  // but this way is clearer, I think.
};


// Class to represent the board in a game of Hex. It is a subclass of "graph" as the neighbour
// relations on the board can be neatly represented as a graph, but we need to add further data and
// additional functionality.
class hexBoard:public graph {

public:
  // Constructor that takes the dimension of the board (always the same in both
  // directions or a very unfair game!). It creates a graph of dimension squared
  // vertices and then sets up the edges that represent the "neighbour" relationships
  // between hexes on the board.
  hexBoard(int dimension):graph(dimension * dimension), dimension(dimension) {
    // Loop through all hexes on the board
    for (int row = 0; row < dimension; ++row) {
      for (int col = 0; col < dimension; ++col) {
	create_hex_edges(row, col);
      }
    }
  }

  // Claims a hex for the blue player, after first checking it is a valid move
  bool blue_move(int row, int col) {
    if (is_valid_move(row, col)) {
      blueVertices.insert(get_vertex_number(row, col));
      return true;
    }
    return false;
  }

  // Claims a hex for the red player, after first checking it is a valid move
  bool red_move(int row, int col) {
    if (is_valid_move(row, col)) {
      redVertices.insert(get_vertex_number(row, col));
      return true;
    }
    return false;
  }

  // Check if the blue player has won - this requires a path of blue vertices from some left edge vertex
  // to some right edge vertex
  bool check_blue_victory() {
    for(int startRow = 0; startRow < dimension; ++startRow) {
      for(int endRow = 0; endRow < dimension; ++endRow) {
	if (shortest_path(get_vertex_number(startRow, 0), get_vertex_number(endRow, dimension - 1), blueVertices) != 0.0) {
	  return true;
	}
      }
    }
    return false;
  }

  // Check if the red player has won - this requires a path of red vertices from some top edge vertex
  // to some bottom edge vertex
  bool check_red_victory() {
    for(int startCol = 0; startCol < dimension; ++startCol) {
      for(int endCol = 0; endCol < dimension; ++endCol) {
	if (shortest_path(get_vertex_number(0, startCol), get_vertex_number(dimension - 1, endCol), redVertices) != 0.0) {
	  return true;
	}
      }
    }
    return false;
  }

  // Display the board on the screen using crude ASCII art
  void display_board() {
    // Print header
    cout << endl << string(3, ' ');
    for (int col = 0; col < dimension; ++col) {
      cout << col << string(3, ' ');
    }
    cout << endl;
    cout << string(3, ' ') << string(3 * (dimension - 1) + dimension, '-') << endl;

    // Print rows of the board
    for (int row = 0; row < dimension; ++row) {
      cout << string(2 * row, ' ') << row << ":" << string(2, ' ');
      for (int col = 0; col < dimension; ++col) {
	cout << get_hex_symbol(row, col) << string(3, ' ');
      }
      cout << ":"  << row << endl;
    }

    // Print footer
    cout << string((2 * dimension) + 3, ' ') << string(3 * (dimension - 1) + dimension, '-') << endl;
    cout << string((2 * dimension) + 3, ' ');
    for (int col = 0; col < dimension; ++col) {
      cout << col << string(3, ' ');
    }
    cout << endl << endl;
  }

  // Check if a specified row and column represents a valid move - i.e. it falls within
  // the confines of the board and is not already claimed by one of the players.
  bool is_valid_move(int row, int col) {
    if ((row < 0) || (row >= dimension) || (col < 0) || (col >= dimension)) {
      return false;
    }

    int vertexNum = get_vertex_number(row, col);
    if ((blueVertices.find(vertexNum) != blueVertices.end()) ||
	(redVertices.find(vertexNum) != redVertices.end())) {
      return false;
    }

    return true;
  }

protected:
  int dimension; // the size of the hex board in each dimension
  set<int> blueVertices; // the vertices already claimed by the blue player
  set<int> redVertices; // the vertices already claimed by the red player

  // Method to create the edges that represent the neighbours of a specified
  // hex. The neighbours are different for corner, edge and middle vertices.
  void create_hex_edges(int row, int col) {
    // Top left corner
    if (row == 0 && col == 0) {
      add_hex_edge(row, col, row, col + 1);
      add_hex_edge(row, col, row + 1, col);
      return;
    }
    // Top right corner
    if (row == 0 && col == (dimension - 1)) {
      add_hex_edge(row, col, row, col - 1);
      add_hex_edge(row, col, row + 1, col);
      add_hex_edge(row, col, row + 1, col - 1);
      return;
    }
    // Top row, not a corner
    if (row == 0) {
      add_hex_edge(row, col, row, col - 1);
      add_hex_edge(row, col, row, col + 1);
      add_hex_edge(row, col, row + 1, col);
      add_hex_edge(row, col, row + 1, col - 1);
      return;
    }
    // Bottom left corner
    if (row == (dimension - 1) && col == 0) {
      add_hex_edge(row, col, row, col + 1);
      add_hex_edge(row, col, row - 1, col);
      add_hex_edge(row, col, row - 1, col + 1);
      return;
    }
    // Bottom right corner
    if (row == (dimension - 1) && col == (dimension - 1)) {
      add_hex_edge(row, col, row, col - 1);
      add_hex_edge(row, col, row - 1, col);
      return;
    }
    // Bottom row, not a corner
    if (row == (dimension - 1)) {
      add_hex_edge(row, col, row, col - 1);
      add_hex_edge(row, col, row, col + 1);
      add_hex_edge(row, col, row - 1, col);
      add_hex_edge(row, col, row - 1, col + 1);
      return;
    }
    // Left column, not a corner
    if (col == 0) {
      add_hex_edge(row, col, row - 1, col);
      add_hex_edge(row, col, row - 1, col + 1);
      add_hex_edge(row, col, row, col + 1);
      add_hex_edge(row, col, row + 1, col);
      return;
    }
    // Right column, not a corner
    if (col == (dimension - 1)) {
      add_hex_edge(row, col, row - 1, col);
      add_hex_edge(row, col, row, col - 1);
      add_hex_edge(row, col, row + 1, col - 1);
      add_hex_edge(row, col, row + 1, col);
      return;
    }
    // And finally, middle vertices
    add_hex_edge(row, col, row - 1, col);
    add_hex_edge(row, col, row - 1, col + 1);
    add_hex_edge(row, col, row, col - 1);
    add_hex_edge(row, col, row, col + 1);
    add_hex_edge(row, col, row + 1, col - 1);
    add_hex_edge(row, col, row + 1, col);
  }

  // Add an edge between two hexes
  void add_hex_edge(int sourceRow, int sourceCol, int targetRow, int targetCol) {
    add_simple_edge(get_vertex_number(sourceRow, sourceCol), get_vertex_number(targetRow, targetCol));
  }

  // Calculate the vertex number corresponding to a hex specified as row and column
  int get_vertex_number(int row, int col) {
    return (row * dimension) + col;
  }

  // Determine the symbol to display on the board for a given hex, depending upon
  // whether it is claimed and, if so, by whom
  char get_hex_symbol(int row, int col) {
    int vertexNum = get_vertex_number(row, col);
    if (blueVertices.find(vertexNum) != blueVertices.end()) {
      return 'X';
    }
    if (redVertices.find(vertexNum) != redVertices.end()) {
      return 'O';
    }
    return '.';
  }
};

 
int main() {
  Board board;
  cout << board;
 
  // play the game by alternating players, read a move, check if the game is over or not.
  while ( true ) {
    position p = board.input_move(state::playerOne);
    cout << board;
    if ( board.is_game_over(p) ) {
      break;
    }
 
    p = board.input_move(state::playerTwo);
    cout << board;
    if ( board.is_game_over(p) ) {
      break;
    }
  }
 
  cout << "Game over!" << endl;
  return 0;
}
