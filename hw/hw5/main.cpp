// Hex game main run
// NB: I tried to follow Google's coding style guidelines
//   i.e. class member variables are postfixed by an underscore
//   (http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml)

#include "hexgame.h"

int main() {
  HexGame::Play();
}
