#include <iostream>
using namespace std;

inline void f(int& x) {
  cout << "x: " << x << endl;
  x = 9;
}

int main() {
  int a = 5;

  f(a);
  cout << "a after f: " << a << endl;

  return 0;
}
