

#include <iostream>
#include <vector>
#include <stdexcept>
#include <cstdlib>//for exit() and EXIT_FAILURE 
//!size of vector
const size_t g_szVec=40;

/**
 * @brief function sums all the elements of the vector
 * @param[in] vector
 * vector whose elements will be summed
 * @ret 
 * result of the sum elements 
 */
template <typename T>
inline T sum( const std::vector<T> &vector)
{
  T result=static_cast<T> (NULL); 
  for(unsigned int i = 0; i < vector.size(); ++i)
    result += vector[i];

  return result;
}

/**
 * main function
 */
int main()
{

  std::vector<int> vector;
  //try to allocate memory
try
  {
    vector.reserve(g_szVec);
  }
 catch(std::bad_alloc)
   {
     std::cerr <<"out of memory"<<std::endl;
     exit(EXIT_FAILURE);
   }
 catch(std::length_error)
   {
     std::cerr <<"lenght error: can't reserve memmory for "<<g_szVec<<" elements"<<std::endl;
     exit(EXIT_FAILURE);
   }

//initialize vector
 for( int i = 0; i < g_szVec; ++i)
   vector.push_back(i);
 //outputting the result of the sum
 std::cout<<"sum is "<<sum( vector)<<std::endl;
   
 return 0;
}
