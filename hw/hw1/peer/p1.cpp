//  Convert this program to C++
//   change to C++ io
//   change to one line comments
//   change defines of constants to const
//   change array to vector<>
//   inline any short function

#include <iostream>
#include <vector>

const int N = 40;

// use everything inside std:: by default
using namespace std;

// the accumulator is passed as reference to the function, thus it's not
// necessary to work with pointers
inline void sum(int &acc, vector<int>& array)
{
  int i;
  acc = 0;
  // use the size() method of the vectro<> class
  for(i = 0; i < array.size(); ++i)
    acc = acc + array[i];
}

int main()
{
  int i;
  int accum = 0;
  // use the std::vector instead of an C array
  vector<int> data(N);

  for(i = 0; i < N; ++i)
    data[i] = i;
  // N doesn't need to be passed to the sum function since it can be derived
  // from the vector class
  sum(accum, data);
  cout << "sum is " << accum << endl;
  return 0;
}
