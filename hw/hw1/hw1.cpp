//  hw1
//  Store consecutive values in a vector of ints
//  Then print out the sum of the values in the vector

#include <iostream>
#include <vector>
using namespace std;

// Size of array, which will store values from 0 to N-1
const int N = 40;

// Add values in d and store in total
inline void sum(int& total, vector<int>& d) {
  total = 0;
  for(int i = 0; i < d.size(); ++i)
    total += d[i];
}

// Main run
int main() {
  // Initialize accumulator
  int accum = 0;

  // Initialize vector values
  vector<int> data(N);
  for(int i = 0; i < N; ++i)
    data[i] = i;

  // Sum values and output results
  sum(accum, data);
  cout << "sum is " << accum << endl;

  return 0;
}
