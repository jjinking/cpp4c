// shortestpath.cc

#include "shortestpath.h"

// Class to enable comparing of second element in std::pair objects
// Used for reversing the order in priority queue
class NodeDistCompare {
public:
  int operator() (const std::pair<int,double>& n1, const std::pair<int,float>& n2) {
    return n1.second > n2.second;
  }
};

// Constructor with initialization list
// Optional parameters for number of nodes and the initial node values
ShortestPath::ShortestPath(UndirectedGraph& g):g_(g) {
  for (int i = 0; i < g_.V(); ++i) {
    v_.push_back(i);
  }
}

// Return list of vertices in the graph
std::list<int> ShortestPath::Vertices() {
  return v_;
}

// Find shortest path between nodes u and w,
// and return the sequence of nodes in the path
// Uses Dijkstra's shortest path algorithm
std::vector<int> ShortestPath::Path(int u, int w) {

  // Declare return variable
  std::vector<int> spath;

  // Declare containers
  std::map<int,bool> visited;
  std::map<int,int> previous;
  std::priority_queue<std::pair<int,double>, std::vector<std::pair<int,double> >, NodeDistCompare> Q;

  // Initialize values for:
  // Distance to each node, visited nodes, and previous nodes
  for (std::list<int>::iterator it=v_.begin(); it != v_.end(); ++it) {
    // Set distance to each node to infinity
    g_.SetNodeValue(*it, std::numeric_limits<double>::infinity());
    visited[*it] = false;
    //previous[*it] = -1; // unnecessary
  }

  // Set distance to source node u to 0
  g_.SetNodeValue(u, 0);
  
  // Insert source into Q
  Q.push(std::make_pair(u,g_.GetNodeValue(u)));

  int current_node;
  while (!Q.empty()) {
    current_node = Q.top().first;
    Q.pop();

    // Mark node as visited
    visited[current_node] = true;

    // End node reached, retrace path
    if (current_node == w) {
      spath.insert(spath.begin(), w);
      int p = previous[w];
      while (p != u) {
	spath.insert(spath.begin(),p);
	p = previous[p];
      }
      spath.insert(spath.begin(), u);
      break;
    }

    // Look at neighboring nodes
    std::vector<int> current_neighbors = g_.Neighbors(current_node);
    double temp;
    for (std::vector<int>::iterator it = current_neighbors.begin(); it != current_neighbors.end(); ++it) {
      temp = g_.GetNodeValue(current_node) + g_.GetEdgeValue(current_node, *it);

      // If new distance to neighboring node is shorter than known dist to that node, update
      if (temp < g_.GetNodeValue(*it) && !visited[*it]) {
	g_.SetNodeValue(*it, temp);
	previous[*it] = current_node;
	Q.push(std::make_pair(*it,g_.GetNodeValue(*it)));
      }
    }
  }
  
  return spath;
}

// Return path cost associated with the shortest path
double ShortestPath::PathSize(int u, int w) {
  double psize = 0;
  std::vector<int> spath = Path(u, w);
  for (std::vector<int>::iterator it = spath.begin(); it != spath.end() - 1; ++it) {
    psize += g_.GetEdgeValue(*it, *(it+1));
  }
  return psize;
}
