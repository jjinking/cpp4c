// shortestpath.h

#ifndef SHORTESTPATH_H
#define SHORTESTPATH_H

#include <limits>
#include <list>
#include <map>
#include <queue>
#include <utility>
#include <vector>
#include "undirectedgraph.h"

// Graph class implementation using adjacency list
// Each node is represented as an integer
class ShortestPath {

 public:

  // Constructor
  ShortestPath(UndirectedGraph& g);

  // Return list of vertices in the graph
  std::list<int> Vertices();

  // Find shortest path between nodes u and w,
  // and return the sequence of nodes in the path
  std::vector<int> Path(int u, int w);
  
  // Return path cost associated with the shortest path
  double PathSize(int u, int w);

 private:
  
  // Graph
  UndirectedGraph g_;

  // Vertices in the graph
  std::list<int> v_;

};

#endif
