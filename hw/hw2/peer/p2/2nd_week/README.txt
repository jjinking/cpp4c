###############################################################################
#                                                                             #
#  This set of files implements the Dijistra algorithm, to find the shortest  #
#  path between to nodes in a randomly generated graph with a given distance  # 
#  range and density.                                                         #
#                                                                             #
#  STRUCTURE:                                                                 #
#    * src: contains the source code and the headers.                         #
#    * obj: contains the *.o files.                                           #
#    * bin: contains the executable (main).                                   #
#                                                                             #
#  COMPILATION: compile the code passing the order make to the command line.  # 
#     the code is already compiled, though.                                   # 
#                                                                             #
#  EXECUTION: the executable is bin/main.                                     #
#                                                                             #
###############################################################################

