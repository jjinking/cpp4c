#include "graph.h"

// graph constructor. It initialises a graph with 
// a MC density rho and a distance range dist. 
// Default parameters are: 
graph::graph(double rho, double dist){
    //use current time as seed for random generator
    srand(std::time(0));
    // Reserve vector of size (int)Nv*Nv 
    adj_matrix.clear();
    adj_matrix.assign(Nv*Nv,0);
    distance=dist;
    density=rho;
    // Loop over the vertices. We run over all edges and 
    // throw a random number p \in [0,1]. If  p < rho 
    // then, we assign the elements Nv*i +j and Nv*j+i of adj_matrix, 
    // a random number between [0,dist]
    for (int i=0; i < Nv; i++){
        for(int j=i+1; j < Nv ; j++){
            double p=static_cast<double> (rand())/RAND_MAX;
            if (p<rho){
                double d = dist*rand()/static_cast<double>(RAND_MAX);
                adj_matrix[Nv*i + j] = d;  
                adj_matrix[Nv*j + i] = d; 
            }
        }
    }    
 
}

// V: returns the number of vertices in the graph
int graph::V(){
    return Nv;
}

// E: returns the number of edges in the graph
int graph::E(){
    int e=0;

    // We count the non zero elements in the upper diagonal
    for (int i=0; i< Nv ; i++){
        for (int j =i+1 ; j< Nv; j++){
            if (adj_matrix[Nv*i + j] != 0) e++; 
        }
    }
    return e;
}

// adjacent(i,j) checks whether there is an edge from node i to node j
// it returns true if it exists and false if not. 
bool graph::adjacent(int i, int j){
    return (adj_matrix[Nv*i+j]!=0);
}

//neighbours(i) lists all nodes j such that there is an edge from i to j. 
vector<int> graph::neighbours(int i){
    vector<int> neigh;
    for (int j = 0 ; j < Nv ; j++){
        if(adj_matrix[Nv*i+j]!=0) neigh.push_back(j);
    }
    return neigh;
} 

//add (i,j): adds to G the edge from i to j, if it is not there.
void graph::add(int i,int j){
    if (adj_matrix[Nv*i + j] == 0){
        double d = distance*rand()/static_cast<double>(RAND_MAX);
        adj_matrix[Nv*i + j] = d;  
        adj_matrix[Nv*j + i] = d; 
    }
}

//delete (G, x, y): removes the edge from i to j, if it is there.
void graph::delet(int i, int j){
    if(adj_matrix[Nv*i + j] !=0 ){
        adj_matrix[Nv*i + j] = 0.0;  
        adj_matrix[Nv*j + i] = 0.0; 
    } 
}

//get_node_value (G, x): returns the value associated with the node x.
int graph::get_node_value(int i){
    return i;
}

//set_node_value( G, x, a): sets the value associated with the node x to a.
// I do not understand why this can be useful

//get_edge_value( G, i, j): returns the value associated to the edge (i,j).
double graph::get_edge_value(int i,int j){
   return adj_matrix[Nv*i + j] ;
}
//set_edge_value (G, x, y, v): sets the value associated to the edge (x,y) to v.
void graph::set_edge_value(int i,int j,double v){
    adj_matrix[Nv*i + j] = v;
    adj_matrix[Nv*j + i] = v;
}
