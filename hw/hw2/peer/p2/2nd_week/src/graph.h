#ifndef GRAPH
#define GRAPH

#include "headers.h"

const int Nv = 50;
using namespace std;

class graph{
    private:
       vector<double> adj_matrix;
       double distance;
       double density; 
    public:
        graph(double rho=0.4, double dist=0.5);
        int V();
        int E();
        bool adjacent(int i, int j);
        vector<int> neighbours(int i) ; 
        void add(int i,int j);
        void delet(int i, int j);
        int get_node_value(int i);
        double get_edge_value(int i,int j);
        void set_edge_value(int i,int j,double v);
        
};
#endif
