#ifndef PRIORITY_QUEUE
#define PRIORITY_QUEUE

#include "headers.h"

using namespace std;
struct elem_open{
    int v1;
    int v2;
    double dist;
};

inline bool operator<(const elem_open& elem1, const elem_open& elem2){
    return(elem1.dist < elem2.dist);
}

inline bool operator==(const elem_open& elem1, const elem_open& elem2){
    return ( ( (elem1.v1==elem2.v1) && (elem1.v2==elem2.v2) ) || 
             ( (elem1.v1==elem2.v2) && (elem1.v2==elem2.v1) ) );
}

inline ostream & operator<<(ostream& out, const elem_open elem){
    out << "(" << elem.v1 << "," << elem.v2 << ',' << elem.dist << ')';
    return out;
    
}

class priority_queue{
    private:
        vector<elem_open> open_set;
    public:
        priority_queue(){ 
            open_set.clear();
        }
        void chgPriority(elem_open node_value); 
        void minPriority();
        bool contains(elem_open queue_elem);
        void insert(elem_open queue_elem);
        elem_open top();
        int size();

        
};
#endif
