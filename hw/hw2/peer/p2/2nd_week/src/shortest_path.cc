#include "shortest_path.h"
// constructor: 
shortest_path::shortest_path(double rho, double dist):
    G(rho,dist) {
        close_set.clear(); 
} 

//vertices(List): list of vertices in G(V,E).
vector<int> shortest_path::vertices(){
    vector<int> vertex;
    for (int i=0;i<G.V(); i++){
        vertex.push_back(i);
    }
    return vertex;
}

// path(u, w): find shortest path between u-w and returns the sequence of vertices representing shorest path u-v1-v2-…-vn-w.
vector<int> shortest_path::path(int u, int w){
    elem_close elem_c; 
    elem_c.v=u; elem_c.sh_path=0; elem_c.path.push_back(u);
    close_set.clear();
    close_set.push_back(elem_c);
    do{
        vector<int> adj_nodes_aux, adj_nodes;
        int x=close_set.back().v;
        adj_nodes_aux = G.neighbours(x);
        for (int j = 0; j< adj_nodes_aux.size(); j++){
            bool flag=false; 
            int i = 0; 
            while ((flag==false) && i< close_set.size()){
               flag = (adj_nodes_aux[j]==close_set[i].v);
               i++; 
            }
            if(!flag) adj_nodes.push_back(adj_nodes_aux[j]);
        }
     //   cout << "" <<adj_nodes_aux.size() << "adj_nodes: "  <<adj_nodes.size()  << endl; 
        for (int j=0; j<adj_nodes.size(); j++){
            int y=adj_nodes[j];
            elem_open elem_o; 
            elem_o.v1=x; elem_o.v2=y; elem_o.dist=G.get_edge_value(x,y); 
            if (! Q.contains(elem_o))  Q.insert(elem_o); 
            else Q.chgPriority(elem_o); 
        }
        if (Q.size() > 0){
            // Extract top element 
            elem_open top = Q.top();
            //cout << "Primer elemento de la lista" << top << endl;
            Q.minPriority();
            // update close set 
            elem_c.v = top.v2;
            for (int i=0 ; i<close_set.size(); i++){
                if (top.v1==close_set[i].v){
                     elem_c.sh_path = close_set[i].sh_path + top.dist;
                     elem_c.path=close_set[i].path;
                     elem_c.path.push_back(top.v2);     
                }
            }
            close_set.push_back(elem_c);
         }
       } while ( (close_set.back()).v != w && Q.size()>0);
    return close_set.back().path;
}
//    path_size(u, w): return the path cost associated with the shortest path.
double shortest_path::path_size(int u,int w){
    if ( close_set.size()==0 || (close_set.front().v!=u || close_set.back().v!=w)){
        vector<int> list = path(u,w);
    }
    //cout << "front: " << close_set.front().v << "back: " <<close_set.back().v << "size: " << close_set.size() << "destination: " << w << endl;
    // return -1 in case there is no path from u to w, so that we can exclude it from the average.
    if (close_set.back().v!=w) return -1.0;
   else return close_set.back().sh_path;
}
