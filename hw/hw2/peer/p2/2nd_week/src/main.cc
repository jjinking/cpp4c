#include <string.h>

#include "headers.h"
#include "graph.h"
#include "priority_queue.h"
#include "shortest_path.h"
 
using namespace std; 


int main(){

    cout << "********************************************************************" << endl;
    cout << "* With this assignment, I learned how to construct a graph, how    *" << endl;
    cout << "* to implement the Dijikstra algorithm, and I got initiated in the *" << endl; 
    cout << "* philosophy of writing code using classes.                        *" << endl; 
    cout << "* The classes created here are:                                    *" << endl;
    cout << "*  - graph: constructs an adjacency matrix to store the info of the*" << endl;
    cout << "*           graph. I  have chosen this representation because the  *" << endl;
    cout << "*           densities we are dealing with are pretty high.         *" << endl;
    cout << "*  - priority_queue: constructs a priority queue, that I use to    *" << endl;
    cout << "*           construct the open set.                                *" << endl; 
    cout << "*  - shortest_path: implements the Dijikstra algorithm.            *" << endl;
    cout << "*                                                                  *" << endl;
    cout << "* See comments in the source code for more details.                *" << endl;
    cout << "********************************************************************" << endl;
    cout << "\n"<<endl;   
 
    cout << "********************************************************************" << endl;
    cout << "* We measure the average path  length on graphs of 50 nodes.       *" << endl;
    cout << "* We compute the 49 paths: 0 to 1, 0 to 2, 0 to 3 ... 0 to 49      *" << endl;
    cout << "* If there is no path between 0 and n, we omit that value from the *" << endl;
    cout << "* average.  We  evaluate the average length for densities ranging  *" << endl; 
    cout << "* [0.2, 0.4] in intervals of 0.05  and distances ranges in         *" << endl;
    cout << "* [1.0, 10.0] in intervals of 0.5                                  *" << endl;
    cout << "********************************************************************" << endl;
    cout << "\n" << endl; 
     

    for (double rho=0.2; rho<0.41 ; rho=rho+0.02){
        for (double dist=1.0 ; dist <=10.0; dist=dist+0.5){
           shortest_path inst_path(rho,dist);
           double avg = 0.0;
           int cc=0;
           for (int i=1; i< Nv ; i++){
               double path_size  = inst_path.path_size(0,i);
               if (path_size >0 ){
                   avg +=path_size;
                   cc++;
               }
           }
           avg = avg/cc;
           cout << "   - Density: "<< setw(5) <<rho << ", Distance range: " << setw(5) <<dist 
                << ", Average  path length: " << setw(10) << avg 
                << ". Existing paths " << cc << " out of 49." <<endl; 
        }
        cout << "    ----------------------------------------------------------------------------------------------------- " << endl;
    }
}
