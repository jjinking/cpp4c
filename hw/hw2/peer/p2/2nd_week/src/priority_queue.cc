#include "priority_queue.h"

//chgPriority(PQ, priority): changes the priority (node value) of queue element.
void priority_queue::chgPriority(elem_open node_value){

    //substitute the new value
    for (int i; i<open_set.size() ; i++){
            if ((open_set[i]==node_value) && (node_value<open_set[i])){
                
                open_set[i].dist = node_value.dist;
            }
    }
    sort(open_set.begin(),open_set.end());
}

//minPrioirty(PQ): removes the top element of the queue.
void priority_queue::minPriority(){
    open_set.erase(open_set.begin());
}
//contains(PQ, queue_element): does the queue contain queue_element.
bool priority_queue::contains(elem_open queue_elem){
    bool var = false;
    int i=0;
    while (var==false && i< open_set.size() ){
       var = (open_set[i]==queue_elem );
       i++;
    }  
    return var;
}

//Insert(PQ, queue_element): insert queue_element into queue
void priority_queue::insert(elem_open queue_elem){
        open_set.push_back(queue_elem);
        sort(open_set.begin(), open_set.end());
}

//size(PQ): return the number of queue_elements.
int priority_queue::size(){
    return open_set.size();
}

//top(PQ):returns the top element of the queue.
elem_open priority_queue::top(){
    return open_set[0];
}


 
