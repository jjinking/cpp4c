#ifndef SHORTEST_PATH
#define SHORTEST_PATH

#include "headers.h"
#include "priority_queue.h"
#include "graph.h"

using namespace std;

struct elem_close{
    int v;
    double sh_path;   
    vector<int> path; 
};

inline ostream & operator<<(ostream& out, const elem_close elem){
    out << "(node, distance) : (" << elem.v << "," << elem.sh_path << ')' ;
    out << " path: ("; 
    for (int i=0; i< elem.path.size() ; i++){
         if(i!=elem.path.size()-1)  out << elem.path[i]<< " - " ;
         else out << elem.path[i] << ")";
    }
    return out;
    
}
class shortest_path{
    private:
        graph G;
        priority_queue Q;
        vector<elem_close> close_set;
    public:
      shortest_path(double rho=0.4,double dist=0.5);
      vector<int> vertices();
      vector<int> path(int u, int w);
      double path_size(int u,int w);
 
        
};
#endif
