//PriorityQueue.h
#ifndef _PRIORITYQUEUE_H_
#define _PRIORITYQUEUE_H_
#include <vector>
using namespace std;
class PriorityQueue
{
 private:
  vector<int> heap;
 public:
  PriorityQueue();
  ~PriorityQueue();
  int DeleMin(const int cost[]);
  void Inserint(int& t);
  bool IsEmpty();
  void print();
};
#endif
