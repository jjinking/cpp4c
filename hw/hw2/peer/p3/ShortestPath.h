//ShortestPath.h
#ifndef _SHORTESTPATH_H_
#define _SHORTESTPATH_H_
#include "Graph.h"
#include "PriorityQueue.h"
class ShortestPath
{
 private:
  Graph &G;
  int *parent;
  int *cost;
  bool *known;
  PriorityQueue heap;
 public:
  ShortestPath(Graph &graph);
  ~ShortestPath();
  void count_graph(int source_node);
  int path(int u,int w);
  int path_size(int u,int w);
  bool is_known(int w);
  void print();
  double Path_average();
};
#endif
