//Graph.cpp
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include "Graph.h"
using namespace std;
Graph::Graph(int node_count,double density)
{
  int tmp;//to conserve the rand number
  double prob;
  srand(time(0));
  size=node_count;
  graph = new int *[size];
  for(int i=0;i<size;++i)
    graph[i]=new int[size];

  for(int i=0;i<size;++i)
    for(int j=i;j<size;++j)
      {
	if(i==j)
	  graph[i][j]=0;//undirected graph
	else
	  {
	    tmp=rand()%10+1;
	    prob=(rand()%100)/100.0;
	    if(density>prob)
	      {
		graph[i][j]=graph[j][i]=tmp;
	      }
	    else
	      graph[i][j]=graph[j][i]=0;
	  }
      }
}

Graph::~Graph()
{
  for(int i=0;i<size;++i)
    delete graph[i];
  delete graph;
}

int Graph::GetSize()
{
  return size;
}

int Graph::GetCost(int v,int w)
{
  return graph[v][w];
}

void Graph::print()
{
  for(int i=0;i<size;i++)
    {
      for(int j=0;j<size;j++)
	cout<<graph[i][j]<<" ";
      cout<<endl;
    }
}

