//Graph.h
#ifndef _GRAPH_H_
#define _GRAPH_H_
class Graph{
 private:
  int ** graph;
  int size;
 public:
  Graph(int node_count,double density); //consintrucintor
  ~Graph();
  //desintrucintor
  int GetSize();
  //return the number of nodes
  int GetCost(int v,int w);
  //return the distance between v and w
  void print();
  //show the imformation for debug
};
#endif
