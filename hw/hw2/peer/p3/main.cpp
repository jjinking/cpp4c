/*
 * Author:scott
 * zhejiang university china
 * 2013.11.3
 */
#include<iostream>
#include "ShortestPath.h"
#include "PriorityQueue.h"
#include "Graph.h"
using namespace std;
int main()
{
  int node_count;
  //set the number of nodes to 50
  double density;
  //set the density to 0.2
  cout<<"please input the number of node:"<<endl;
  cin>>node_count;
  cout<<"please input the density of the graph:"<<endl;
  cin>>density;

  Graph G(node_count,density);
  //create a graph random
  ShortestPath sp(G);
  //pass the Graph G to ShortestPath
  sp.count_graph(0);
  //to solve the problem and the source node is 0
  for(int i=1;i<node_count;i++) //print the information
    {
      cout<<"path:";
      sp.path(0,i);
      cout<<endl;
      cout<<"distance between 0 and "<<i<<": "<<sp.path_size(0,i);
      cout<<endl;
    }
  cout<<"the average path cost is "<<sp.Path_average()<<endl;
  return 0;
}
