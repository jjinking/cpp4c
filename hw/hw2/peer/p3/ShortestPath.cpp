//ShortestPath.cpp
#include<iostream>
#include "ShortestPath.h"
using namespace std;
ShortestPath::ShortestPath(Graph &graph):G(graph)
{
  //G=graph;
  int size=graph.GetSize();
  //create variables
  parent = new int[size];
  cost = new int[size];
  known = new bool[size];
  //initialize the variable
  for(int i=0;i<size;++i)
    {
      parent[i] = -1;//-1 meas no parent
      cost[i] = -1;//-1 meas unreachable
      known[i]=false;
    }
}

ShortestPath::~ShortestPath()
{
  delete [] parent;
  delete [] cost;
  delete [] known;
}
int ShortestPath::path(int u,int w)
{
  if(parent[w]==-1)
    {
      cout<<"The node "<<w<<" is unreachable!"<<endl;
      return -1;
    }
  if(u==w)
    cout<<u;
  if(u!=w&&path(u,parent[w])!=-1)
    cout<<"->"<<w;
  return 0;
}
int ShortestPath::path_size(int u,int w)
{
  return cost[w];
}
bool ShortestPath::is_known(int w)
{
  return known[w];
}

void ShortestPath::count_graph(int source_node)
{
  int current;
  int distance;
  cost[source_node]=0;
  parent[source_node]=0;//itself
  heap.Inserint(source_node);
  while(!heap.IsEmpty())
    {
      for(;;)
	{
	  current=heap.DeleMin(cost);
	  if(current<0)
	    return;
	  if(!known[current])
	    break;
	}
      known[current]=true;
      for(int i=0;i<G.GetSize();i++)
	{
	  distance=G.GetCost(current,i);
	  if(distance>0)//for each i adjacent to current
	    if(!known[i]&&((cost[i]>cost[current]+distance)||cost[i]==-1))
	      {
		cost[i]=cost[current]+distance;
		parent[i]=current;
		heap.Inserint(i);
		//print();
	      }
	}

    }
}
void ShortestPath::print()
{
  cout<<"cost:";
  for(int i=0;i<G.GetSize();i++)
    cout<<cost[i]<<" ";
  cout<<endl;
  cout<<"parent:";
  for(int i=0;i<G.GetSize();i++)
    cout<<parent[i]<<" ";
  cout<<endl;
  cout<<"known:";
  for(int i=0;i<G.GetSize();i++)
    cout<<known[i]<<" ";
  cout<<endl;
  cout<<heap.IsEmpty()<<endl;
}

double ShortestPath::Path_average()
{
  double sum=0;
  int count=0;
  for(int i=0;i<G.GetSize();i++)
    if(cost[i]!=-1)
      {
	sum+=cost[i];
	count++;
      }
  return sum/count;
}


