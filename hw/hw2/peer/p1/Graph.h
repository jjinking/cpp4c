#pragma once
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
 * number of vertex of graph. 1 extra is for
 *Edge[0][*] and Edge[*][0] and also vertexes[0]
 */
const int N = 50;

using namespace std;

/*
 *this is a class for include an Edge of our graph
 *it has source , target and dist.
 *source and target is for it's target and source
 *and dist include the cost of this edge
 */

class GraphEdge
{
public:
    int source , target; 
	double dist;
};

/*
 *this is a class for include a vertex of our graph
 *it has a name and size. name is for name of this
 *and size include how much edges is connected to it
 */
class GraphVertex
{
public:
    int name ; 
	int szie ;
	GraphVertex();
};

class Graph
{
public:
    double density; //percent of Edges of K-50 Graph in our problem
    /*
     *include all Edges of graph. for easier each
     *Edge use 2 times. but tis is not matrix
     */
    GraphEdge Edges[N][N];
    GraphVertex Vertexes[N];//include all vertexes of graph
    Graph(double Gdensity); //init Graph
    double* neighbors (int x);
};
