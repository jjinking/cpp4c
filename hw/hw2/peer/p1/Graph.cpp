#include "Graph.h"

/*
 *init our graph
 *use rand number for distance of random Edges!
 *when distance of an Edge is 0 means that
 *this Edge does not exist
 */
Graph::Graph(double Gdensity)
{
	density = Gdensity;
	
    for(int i = 0 ; i <50 ; i++) //set new graph
    {
        Vertexes[i].name = i; //set vertexes names
        Vertexes[i].szie = 0; //det default degree of each vertex

        for(int j = 0 ; j <50 ; j++)
        {
            Edges[i][j].source = i; //set the new edge but it don't mean it exist!
            Edges[i][j].target = j;

			Edges[j][i].source = j;
			Edges[j][i].source = i;
			//int RAND = rand();
			if(Edges[i][j].dist == 0 && Edges[i][j].dist == 0 && i != j) //if the edge already not exist
            {
				if(static_cast<double>(rand())/static_cast<double>(RAND_MAX) < density && Vertexes[i].szie<50 && Vertexes[i].szie<50 )
                {
					Edges[j][i].dist = Edges[i][j].dist =(static_cast<double>(rand())/static_cast<double>(RAND_MAX) * 9.0)+1.0; //set the distance
                    Vertexes[i].szie ++; // set new degrees for target and source vertex
                    Vertexes[j].szie ++;
                }
                else
                    Edges[i][j].dist = Edges[j][i].dist = 0.0; //if the edge not exist set distance 0
            }
            else
                Edges[i][j].dist = Edges[j][i].dist = 0.0; 
        }
    }
}

/*
 *return an array with size of number of vetexes.
 *if the Edge exist with target of neigh[i] returns true
 *but if it dosen't exist retruns false
 */
double* Graph::neighbors(int x)
{
	double *neigh = new double[50];
    for(int i = 0 ; i<50 ; i++)
    {		
		neigh[i] = Edges[x][i].dist;
    }
    return neigh;
}

GraphVertex::GraphVertex()
{
	GraphVertex::szie = 0;
}
