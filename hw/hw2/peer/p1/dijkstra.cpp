#include "dijkstra.h"

dijkstra::dijkstra(Graph* G) //init dijkstra algo
{
	g = G;
	ave = 0.0;
	for(int i=0 ; i<50 ; i++)
	{
		path[0][i] = 0.0; //path
		path[1][i] = 0.0; //visited
	}
}

double* dijkstra::shortest() //find shortest path from 0th vertex to others
{
	int next = 0; //next and prev processed veretx
	double *MIN = new double[50]; //hold the final result

	for(int i = 0 ; i<50 ; i++) //calc the shortest path for 50 times
	{
		if(i == 0) //first vertex
		{
			double *costs = new double[50];
			costs = g->neighbors(0); //get the neighbors of first vertex

			for (int n = 0; n < 50; n++) //set the path for all of Edges that relates to 0th vertex
			{
				if(costs[n] != 0)
					path[0][n] = costs[n];	
				else
					path[0][n] = 0.0;	
			}
			double least = 10;
			for (int m = 0; m < 50; m++) //calc the next vertex for algo
			{
				if(path[0][m] <= least && path[1][m] == 0 && path[0][m]> 0)
				{
					least = path[0][m];					
					next = m;
				}
			}
			path[1][next] = 1.0;
		}
		else //next vertexex
		{
			double *costs = new double[50];
			costs = g->neighbors(next); //get the neighbors of next vertex

			for (int n = 0; n < 50; n++) //set or update the new shortest path
			{
				if(costs[n] != 0)
				{
					if(path[0][n] == 0)
						path[0][n] = path[0][next] + costs[n];
					else
					{
						if(path[0][n] > path[0][next] + costs[n])
							path[0][n] = path[0][next] + costs[n];
					}
				}
			}

			double least = 10;
			for (int m = 0; m < 50; m++) //calc the next vertex for algo
			{ 
				if(path[0][m] < least && path[1][m] == 0 && path[0][m]> 0)
				{
					least = path[0][m];					
					next = m;
				}				
			}
			path[1][next] = 1.0;
		}
	}

	for (int i = 0; i < 50; i++) //set the results
	{
		if (i == 0)
			MIN[0] = 0.0;
		else
			MIN[i] = path[0][i];
	}
	return MIN; //return result
}

void dijkstra::print() //print the result
{
	int number = 0;
	double *MIN = new double[50];
	MIN = shortest();
	for (int f = 0; f < 50; f++)
	{
		if(MIN[f] != 0)
		{
			ave += MIN[f];
			number ++;
		}
	}
	ave = ave / number;
}