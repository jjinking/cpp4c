// HW 2
// Implement Dijkstra's algorithm on graphs
// Graph class is implemented as adjacency matrix to keep track of adjacent nodes
//   A 2-dimensional std::map is used as a sparse matrix for the adjacency matrix
// NB: I tried to follow Google's coding style guidelines
//   i.e. class member variables are postfixed by an underscore
//   (http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml)

#include <ctime>
#include <iostream>
#include "undirectedgraph.h"
#include "shortestpath.h"

using namespace std;

void PrintVector(const vector<int>& v) {
  for (vector<int>::const_iterator i = v.begin(); i != v.end(); ++i)
    cout << *i << " ";
}

void TestGraph() {
  // Test initialization and number of nodes and edges
  cout << "Initializing graph with 5 nodes" << endl;
  UndirectedGraph g = UndirectedGraph(5);
  cout << "n nodes: " << g.V() << endl;
  cout << "n edges: " << g.E() << endl;
  cout << endl;

  // Test adding edges
  cout << "Testing AddEdge(x,y)" << endl;
  cout << "Adding and edge between nodes 2 and 3" << endl;
  g.AddEdge(2,3);
  cout << "Adding edge between nodes 0 and 4" << endl;
  g.AddEdge(0,4);
  cout << "n nodes: " << g.V() << endl;
  cout << "n edges: " << g.E() << endl;
  cout << endl;
  cout << "Adding edge between nodes 1 and 2" << endl;
  g.AddEdge(1,2);
  cout << "Adding edge between nodes 2 and 4" << endl;
  g.AddEdge(2,4);
  cout << "n nodes: " << g.V() << endl;
  cout << "n edges: " << g.E() << endl;
  cout << endl;

  // Test IsAdjacent
  cout << "Testing IsAdjacent()" << endl;
  cout << "Nodes 2 and 3 are adjacent: " << g.IsAdjacent(2,3) << endl;
  cout << "Nodes 1 and 4 are adjacent: " << g.IsAdjacent(1,4) << endl;
  cout << "Nodes 2 and 4 are adjacent: " << g.IsAdjacent(2,4) << endl;
  cout << "Nodes 4 and 2 are adjacent: " << g.IsAdjacent(4,2) << endl;
  cout << "Nodes 2 and 1 are adjacent: " << g.IsAdjacent(2,1) << endl;
  cout << endl;
  
  // Test Neighbors
  cout << "Testing Neighbors()" << endl;
  cout << "Neighbors of 0: ";
  PrintVector(g.Neighbors(0));
  cout << endl;
  cout << "Neighbors of 4: ";
  PrintVector(g.Neighbors(4));
  cout << endl;
  cout << "Neighbors of 2: ";
  PrintVector(g.Neighbors(2));
  cout << endl;
  cout << "Neighbors of 1: ";
  PrintVector(g.Neighbors(1));
  cout << endl << endl;
  
  // Test DeleteEdge
  cout << "Testing DeleteEdge()" << endl;
  cout << "Deleting edge (4,2)" << endl;
  g.DeleteEdge(4,2);
  cout << "n nodes: " << g.V() << endl;
  cout << "n edges: " << g.E() << endl;
  cout << "Neighbors of 2: ";
  PrintVector(g.Neighbors(2));
  cout << endl;
  cout << "Neighbors of 0: ";
  PrintVector(g.Neighbors(0));
  cout << endl;
  cout << "IsAdjacent(2,4): " << g.IsAdjacent(2,4) << endl;
  cout << endl;
  cout << "Deleting edge (2,3)" << endl;
  g.DeleteEdge(2,3);
  cout << "n nodes: " << g.V() << endl;
  cout << "n edges: " << g.E() << endl;
  cout << "Neighbors of 2: ";
  PrintVector(g.Neighbors(2));
  cout << endl;
  cout << "Neighbors of 3: ";
  PrintVector(g.Neighbors(3));
  cout << endl;
  cout << "IsAdjacent(3,2): " << g.IsAdjacent(2,4) << endl;
  cout << endl;

  // Re-add edges (2,4) and (2,3)
  cout << "Re-adding edges (2,4) and (2,3)" << endl << endl;;
  g.AddEdge(2,4);
  g.AddEdge(2,3);

  // Test GetNodeValue and SetNodeValue
  cout << "Testing GetNodeValue and SetNodeValue" << endl;
  cout << "Value of node 0: " << g.GetNodeValue(0) << endl;
  cout << "Setting value of node 0 to 100 " << endl;
  g.SetNodeValue(0,100);
  cout << "Value of node 0: " << g.GetNodeValue(0) << endl;
  
  cout << "Value of node 2: " << g.GetNodeValue(2) << endl;
  cout << "Setting value of node 2 to 200 " << endl;
  g.SetNodeValue(2,200);
  cout << "Value of node 2: " << g.GetNodeValue(2) << endl;
  cout << endl;

  // Test GetEdgeValue SetEdgeValue
  cout << "Testing GetEdgeValue and SetEdgeValue" << endl;
  cout << "Value of edge (2,4): " << g.GetEdgeValue(2,4) << endl;
  cout << "Setting value of edge (2,4) to 4.5" << endl;
  g.SetEdgeValue(2,4,4.5);
  cout << "Value of edge (2,4): " << g.GetEdgeValue(2,4) << endl;
  cout << "Value of edge (2,3): " << g.GetEdgeValue(2,3) << endl;
  cout << "Setting value of edge (2,3) to 7.68" << endl;
  g.SetEdgeValue(2,3,7.68);
  cout << "Value of edge (2,3): " << g.GetEdgeValue(2,3) << endl;
}

void TestDijkstra() {
  // First build graph
  UndirectedGraph g = UndirectedGraph(8);
  g.AddEdge(0,1,2);
  g.AddEdge(0,2,5);
  g.AddEdge(0,3,4);
  g.AddEdge(1,2,2);
  g.AddEdge(1,4,7);
  g.AddEdge(1,6,12);
  g.AddEdge(2,3,1);
  g.AddEdge(2,4,4);
  g.AddEdge(2,5,3);
  g.AddEdge(3,5,4);
  g.AddEdge(4,5,1);
  g.AddEdge(4,7,5);
  g.AddEdge(5,7,7);
  g.AddEdge(6,7,3);
  
  ShortestPath sp = ShortestPath(g);
  vector<int> p = sp.Path(0,7);
  
  cout << "Path: ";
  PrintVector(p);
  cout << endl;
  cout << "Size: " << sp.PathSize(0,7) << endl;

}

// Generate graph with random edges
UndirectedGraph GenerateRandomEdgeGraph(int num_nodes, double edge_density, double distance_range) {
  double prob;
  double dist;
  UndirectedGraph g = UndirectedGraph(num_nodes);

  // Generate "random" edges
  for (int i = 0; i < num_nodes; ++i) {
    for (int j = 0; j < i; ++j) {
      prob = rand() / double(RAND_MAX);
      if (prob < edge_density) {
	dist = distance_range * rand() / double(RAND_MAX);
	g.AddEdge(i, j, dist);
      }
    }
  }
  return g;
}

// Compute average path length
double ComputeAvgPathLen(UndirectedGraph& ug) {
  double total_len = 0;
  int numpaths = 0;
  for (int i = 0; i < ug.V(); ++i) {
    for (int j = 0; j < i; ++j) {
      ShortestPath sp(ug);
      if (sp.Path(i,j).size() > 0) {
	total_len += sp.PathSize(i,j);
	numpaths += 1;
      }
    }
  }
  return total_len / numpaths;
}

// Create a randomly generated graph with given density
// and distance ranges from 1.0 to 10.0.
// Write average path lengths to standard output
void SimulateAvgPathLens (int num_nodes, double density) {
  for (double dr = 1.0; dr < 10.1; dr += 1.0) {
    UndirectedGraph g = GenerateRandomEdgeGraph(num_nodes, density, dr);
    cout << "\tAvg distance for distance range " << dr << ": " << ComputeAvgPathLen(g) << endl;
  }
}

int main() {
  //TestGraph();
  //TestDijkstra();

  srand(time(0));

  // Create graph of 50 nodes
  int num_nodes = 50;

  cout << "Density 20%" << endl;
  SimulateAvgPathLens(num_nodes, 0.20);
  cout << endl;

  cout << "Density 40%" << endl;
  SimulateAvgPathLens(num_nodes, 0.40);
}
