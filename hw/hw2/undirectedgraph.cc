// undirectedgraph.cc

#include "undirectedgraph.h"

// Constructor with initialization list
// Optional parameters for number of nodes and the initial node values
UndirectedGraph::UndirectedGraph(int num_nodes, double init_val)
 :n_nodes_(num_nodes), n_edges_(0) {
  // Set initial node values
  for (int i = 0; i < num_nodes; ++i) {
    node_val_[i] = init_val;
  }
}

// Tests whether there is an edge from node x to node y
bool UndirectedGraph::IsAdjacent(int x, int y) {
  return graph_.find(x) != graph_.end() && graph_[x].find(y) != graph_[x].end();
}

// Return vector of all y such that there is an edge from node x to node y
std::vector<int> UndirectedGraph::Neighbors(int x) {
  std::vector<int> connected_nodes;
  if (x < n_nodes_) {
    for (std::map<int,double>::iterator it = graph_[x].begin(); it != graph_[x].end(); ++it) {
	connected_nodes.push_back(it->first);
    }
  }
  return connected_nodes;
}

// Add an edge with value v between nodes x,y
void UndirectedGraph::AddEdge(int x, int y, int v) {
  if (x < n_nodes_ && y < n_nodes_) {
    graph_[x][y] = v;
    graph_[y][x] = v;
    n_edges_++;
  }
}

// Delete an edge from x to y, if it exists
void UndirectedGraph::DeleteEdge(int x, int y) {
  if (IsAdjacent(x,y)) {
    graph_[x].erase(y);
    graph_[y].erase(x);
    n_edges_--;
  }
}

// Return value associated with node x
// If node doesn't exist, return -1
double UndirectedGraph::GetNodeValue(int x) {
  if (x < n_nodes_) {
    return node_val_[x];
  }
  return -1;
}

// Set value associated with node x to v
void UndirectedGraph::SetNodeValue(int x, double v) {
  if (x < n_nodes_) {
    node_val_[x] = v;
  }
}

// Return value associated with edge (x,y)
double UndirectedGraph::GetEdgeValue(int x, int y) {
  return graph_[x][y];
}

// Set value associated with edge (x,y) to v
void UndirectedGraph::SetEdgeValue(int x, int y, double v) {
  if (x < n_nodes_ && y < n_nodes_ && IsAdjacent(x,y)) {
    graph_[x][y] = v;
    graph_[y][x] = v;
  }
}
