// hexgame.h

#ifndef HEXGAME_H
#define HEXGAME_H

#include <iostream>
#include "ugraph.h"

class HexGame {

  enum class BoardSize : int { SMALL = 7, LARGE = 11 }; 
  enum class UserColor : int { BLUE = 1, RED = 2 };

 public:
  
  // Constructor
  HexGame(BoardSize board_size=BoardSize::SMALL, UserColor user_color=UserColor::BLUE);

  // Draw board using ASCII symbols
  void Draw();

  // Determine if a move is legal
  bool IsMoveLegal(int i, int j);

  // Make a move
  void MakeMove(UserColor user_color);

  // Check if given player won the game
  bool CheckPlayerWon(UserColor user_color);

  // Run game
  static int Play();

 private:

  // Return node id number from board coordinates
  int Convert2NodeNumber(int row, int col);

  // Convert UserColor value to double value
  double ConvertUser2Double(UserColor ucolor);

  // Check to see if node's value is set to a color
  double IsNodeSet2Color(int row, int col, UserColor ucolor);

  // Size of board
  int board_size_;
  
  // Player's color
  UserColor user_color_;
  
  // Graph representation of the hex board
  UndirectedGraph graph_;
};

#endif
