// ugraph.cc

#include "ugraph.h"

// Class to enable comparing of second element in std::pair objects
// Used for reversing the order in priority queue
class NodeDistCompare {
public:
  int operator() (const std::pair<std::pair<int,int>,double>& n1,
		  const std::pair<std::pair<int,int>,double>& n2) {
    return n1.second > n2.second;
  }
};

// Constructor with initialization list
// Optional parameters for number of nodes and the initial node values
UndirectedGraph::UndirectedGraph(int num_nodes, double init_val)
 :n_nodes_(num_nodes), n_edges_(0) {
  // Set initial node values
  for (int i = 0; i < num_nodes; ++i) {
    node_val_[i] = init_val;
  }
}

// Constructor that can load a graph from file
UndirectedGraph::UndirectedGraph(std::ifstream &fin, double init_val): n_edges_(0) {

  // Read first line and initialize empty graph
  fin >> n_nodes_;
  for (int i = 0; i < n_nodes_; ++i) {
    node_val_[i] = init_val;
  }

  // Add edges
  int i,j;
  double cost;
  while(fin >> i >> j >> cost) {
    AddEdge(i, j, cost);
  }
}

// Tests whether there is an edge from node x to node y
bool UndirectedGraph::IsAdjacent(int x, int y) {
  return graph_.find(x) != graph_.end() && graph_[x].find(y) != graph_[x].end();
}

// Return vector of all y such that there is an edge from node x to node y
std::vector<int> UndirectedGraph::Neighbors(int x) {
  std::vector<int> connected_nodes;
  if (x < n_nodes_) {
    for (std::map<int,double>::iterator it = graph_[x].begin(); it != graph_[x].end(); ++it) {
	connected_nodes.push_back(it->first);
    }
  }
  return connected_nodes;
}

// Add an edge with value v between nodes x,y
void UndirectedGraph::AddEdge(int x, int y, int v) {
  if (x < n_nodes_ && y < n_nodes_) {
    graph_[x][y] = v;
    graph_[y][x] = v;
    n_edges_++;
  }
}

// Delete an edge from x to y, if it exists
void UndirectedGraph::DeleteEdge(int x, int y) {
  if (IsAdjacent(x,y)) {
    graph_[x].erase(y);
    graph_[y].erase(x);
    n_edges_--;
  }
}

// Return value associated with node x
// If node doesn't exist, return -1
double UndirectedGraph::GetNodeValue(int x) {
  if (x < n_nodes_) {
    return node_val_[x];
  }
  return -1;
}

// Set value associated with node x to v
void UndirectedGraph::SetNodeValue(int x, double v) {
  if (x < n_nodes_) {
    node_val_[x] = v;
  }
}

// Return value associated with edge (x,y)
double UndirectedGraph::GetEdgeValue(int x, int y) {
  return graph_[x][y];
}

// Set value associated with edge (x,y) to v
void UndirectedGraph::SetEdgeValue(int x, int y, double v) {
  if (x < n_nodes_ && y < n_nodes_ && IsAdjacent(x,y)) {
    graph_[x][y] = v;
    graph_[y][x] = v;
  }
}

// Add up the endge costs of a graph
double UndirectedGraph::TotalEdgeCost() {
  double total = 0;
  for (int i = 0; i < n_nodes_; ++i)
    for (int j = 0; j < i; ++j)
      if (IsAdjacent(i,j))
	total += GetEdgeValue(i,j);
  return total;
}

// Compute MST using Prim's algorithm
UndirectedGraph* UndirectedGraph::PrimMST() {

  // Keep track of connected nodes as well as edges
  // Edges is necessary since my graph class keeps track of nodes with
  // auto-incrementing ids, and therefore I cannot add nodes to my tree
  // with the correct id
  std::set< std::pair<int,int> > connected_edges;
  std::set<int> connected_nodes;
  connected_nodes.insert(0);

  // Loop until all nodes are added
  // Assuming this graph is connected (see hw specs)
  std::pair<int,int> min_dist_pair;
  while (static_cast<int>(connected_nodes.size()) < n_nodes_) {
    // Declare priority queue for selecting minimum distance node
    std::priority_queue<std::pair<std::pair<int,int>, double>, 
			std::vector<std::pair<std::pair<int,int>, double> >,
			NodeDistCompare> priority_queue;

    // Select minimum distance neighboring node
    for (auto connected_node: connected_nodes) {
      for (auto neighbor: Neighbors(connected_node)) {
	// Only insert non-connected nodes into priority queue
	if (connected_nodes.find(neighbor) == connected_nodes.end()) {
	  priority_queue.push(std::make_pair(std::make_pair(connected_node,
							    neighbor),
					     GetEdgeValue(connected_node, neighbor)));
	}
      }
    }
    // Add min distance node to list of edges
    min_dist_pair = priority_queue.top().first;
    connected_nodes.insert(min_dist_pair.second);
    connected_edges.insert(min_dist_pair);
  }
  
  // Set MST tree and MST cost
  UndirectedGraph *mst = new UndirectedGraph(n_nodes_);
  int x,y;
  double v;
  for (auto edge: connected_edges) {
    x = edge.first;
    y = edge.second;
    v = GetEdgeValue(x,y);
    mst->AddEdge(x, y, v);
  }
  return mst;
}

// Check to see if two nodes are connected
// Use DFS to see if node y is connected to node x
bool UndirectedGraph::IsConnected(int x, int y) {
  std::set<int> discovered;
  std::stack<int> s;
  discovered.insert(x);
  s.push(x);
  int curnode;
  while (!s.empty()) {
    curnode = s.top();
    s.pop();

    // If y is found
    if (curnode == y)
      return true;
    // Search neighbors
    for (auto neighbor: Neighbors(curnode)) {
      if (discovered.find(neighbor) == discovered.end()) {
	s.push(neighbor);
	discovered.insert(neighbor);
      }
    }
  }
  return false;
}
