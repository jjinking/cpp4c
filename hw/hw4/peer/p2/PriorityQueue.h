#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

#include <vector>
#include <exception>

using namespace std;

/*
* Implementation of a min priority queue based on the 
* book "Algorithms, 4th Edition" Robert Sedgewick and Kevin Wayne
*/
template<class T>
class PriorityQueue {
private:
	int m_maxNum;		// maximum number of elements on PQ
	int m_N;			// number of elements on PQ
	vector<int> pq;		// binary heap using 1-based indexing
	vector<int> qp;		// inverse of pq - qp[pq[i]] = pq[qp[i]] = i
	vector<T> keys;		// keys[i] = priority of i

	// compare keys
	bool greater(int i, int j) {
		return keys[pq[i]] > keys[pq[j]];
	}

	// swap keys
	void exch(int i, int j) {
		int swap = pq[i]; 
		pq[i] = pq[j]; 
		pq[j] = swap;
		qp[pq[i]] = i; 
		qp[pq[j]] = j;
	}

	// let the key swim to the top
	void swim(int k)  {
		while (k > 1 && greater(k/2, k)) {
			exch(k, k/2);
			k = k/2;
		}
	}

	// let the key sink to the bottom
	void sink(int k) {
		while (2*k <= m_N) {
			int j = 2*k;
			if (j < m_N && greater(j, j+1)) j++;
			if (!greater(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

public:
	// constructor
	PriorityQueue(int maxNum) : m_maxNum(maxNum), m_N(0) {
		if (maxNum < 0)
			throw invalid_argument("Size of the priority queue must be non-negative.");
		// allocate internal arrays
		keys.resize(m_maxNum+1);
		pq.resize(m_maxNum+1, -1);
		qp.resize(m_maxNum+1, -1);
	}

	// is the priority	queue empty?
	bool isEmpty() {
		return m_N == 0;
	}
	
     // is i an index on the priority queue?
    bool contains(int i) {
		if (i < 0 || i >= m_maxNum) 
			throw invalid_argument("Index out of bounds");
        return qp[i] != -1;
    }

	// return the number of queue_elements.
	int size() {
		return m_N;
	}
	
	// insert key associated with index i into the priority queue
	void insert(int i, T key) {
		if (i < 0 || i >= m_maxNum) 
			throw invalid_argument("Index out of bounds");
		if (contains(i)) 
			throw invalid_argument("Index is already in the priority queue");
		m_N++;			// increase number of keys
		qp[i] = m_N;	// associate the key with the i'th element of the PQ
		pq[m_N] = i;	// assign the corresponding value to the inverse of PQ
		keys[i] = key;	// store the key value at the bottom of the PQ
		swim(m_N);		// let the key find it right position in the PQ
	}
	
    // return the index associated with the minimal key.
    int minIndex() { 
        if (m_N == 0) 
			throw invalid_argument("Priority queue underflow");
        return pq[1]; // 1'st element has the minimal key       
    }

    // return the minimal key.
    T minKey() { 
        if (m_N == 0) 
			throw invalid_argument("Priority queue underflow");
        return keys[pq[1]];   // 1'st element has the minimal key     
    }

    // delete the minimal key and return its associated index.
    int delMin() { 
        if (m_N == 0) 
			throw invalid_argument("Priority queue underflow");
        int min = pq[1];			// temp save the minimum       
        exch(1, m_N--);				// exchange 1st and last elements
        sink(1);					// let the 1st element sink to its correct position
        qp[min] = -1;				// delete
        //keys[pq[m_N+1]] = null;  // to help with garbage collection
        //pq[m_N+1] = -1;          // not needed
        return min; 
    }

    // return the key associated with index i.
    T keyOf(int i) {
        if (i < 0 || i >= m_maxNum) 
			throw invalid_argument("Index out of bounds");
        if (!contains(i)) 
			throw invalid_argument("Index is not in the priority queue");
        else return keys[i];
    }

    // change the key associated with index i to the specified value.
    void changeKey(int i, T key) {
		if (i < 0 || i >= m_maxNum) 
			throw invalid_argument("Index out of bounds");
		if (!contains(i)) 
			throw invalid_argument("Index is not in the priority queue");
        keys[i] = key;
        swim(qp[i]);
        sink(qp[i]);
    }

    // decrease the key associated with index i to the specified value.
    void decreaseKey(int i, T key) {
		if (i < 0 || i >= m_maxNum) 
			throw invalid_argument("Index out of bounds");
		if (!contains(i)) 
			throw invalid_argument("Index is not in the priority queue");
		keys[i] = key;
        swim(qp[i]);
    }

    // increase the key associated with index i to the specified value.
    void increaseKey(int i, T key) {
		if (i < 0 || i >= m_maxNum) 
			throw invalid_argument("Index out of bounds");
		if (!contains(i)) 
			throw invalid_argument("Index is not in the priority queue");
		keys[i] = key;
        sink(qp[i]);
    }

    // delete the key associated with index i.
    void deleteKey(int i) {
		if (i < 0 || i >= m_maxNum) 
			throw invalid_argument("Index out of bounds");
		if (!contains(i)) 
			throw invalid_argument("Index is not in the priority queue");
		int index = qp[i];
        exch(index, m_N--);
        swim(index);
        sink(index);
        //keys[i] = NULL;
        qp[i] = -1;
    }
};

#endif
