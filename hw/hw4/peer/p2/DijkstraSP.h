#ifndef DIJKSTRASP_H
#define DIJKSTRASP_H

#include <vector>
#include <queue>
#include <limits>
#include <stack>
#include "graph.h"
#include "edge.h"
#include "PriorityQueue.h"

using namespace std;

/*
* Implementation of the Dijkstra Algorithm is based on the 
* book "Algorithms, 4th Edition" Robert Sedgewick and Kevin Wayne
*/
class ShortestPath {
private:
	int m_source;				// source vertex	
	vector<double> m_distTo;	// distTo[v] = distance of the shortest path s->v
	vector<int> m_edgeTo;		// edgeTo[v] = last vertex on shortest path s->v for reconstruction	
	PriorityQueue<double> m_pq;
	
	// recurse the search on vertex connected to v via Edge e
	void recurseOn(int v, Edge & e){
		int w = e.other(v);
		// update the path length to vertex w if necessary
		if (m_distTo[w] > m_distTo[v] + e.weight()) { 
			m_distTo[w] = m_distTo[v] + e.weight();
			m_edgeTo[w] = v;
			if (m_pq.contains(w))					// vertex w is on the PQ
				m_pq.decreaseKey(w, m_distTo[w]);	// update the the distance
			else									// vertex w is not on the PQ
				m_pq.insert(w, m_distTo[w]);		// put the vertex on the PQ
		}
	}
public:
	ShortestPath(UndirectedGraph & G, int source) 
		: m_source(source), 
		m_pq(PriorityQueue<double>(G.V())) {

		// check if input is valid: edges must be non-negative
		list<tr1::shared_ptr<Edge> > edges = G.allEdges();
		for (list<tr1::shared_ptr<Edge> >::iterator it = edges.begin(); it != edges.end(); it++) {
			if ((*it)->weight() < 0)
				throw invalid_argument("Negative edge lengths detected. Dijkstra only works for non-negative edge length graphs");
		}

		// allocate space for internal arrays and initialise the Dijkstra algorithm: 
		// set SP lengths to Inf.	
		m_distTo.resize(G.V(), numeric_limits<double>::infinity());
		m_edgeTo.resize(G.V(),-1);	

		// Path legnth from source to source is 0
		m_distTo[source] = 0.0;		
		m_pq.insert(source, m_distTo[source]);

		while (!m_pq.isEmpty()) {					// process all vertices until PQ is not empty	
			int v = m_pq.delMin();					// remove the minimum vertex from the PQ	
			list<tr1::shared_ptr<Edge> > adjEdges = G.adjEdges(v);	// get all its adjacent edges 
			list<tr1::shared_ptr<Edge> >::iterator it = adjEdges.begin();
			for (; it != adjEdges.end(); it++) {	// iterate over all adjacent vertices				
					recurseOn(v, **it);
			}								
		}
	}

	// length of shortest path from s to v
	double distTo(int v) {
		return m_distTo[v];
	}

	// is there a path from s to v?
	bool hasPathTo(int v) {
		return m_distTo[v] < std::numeric_limits<double>::infinity();
	}

	// shortest path from s to v as an Iterable, null if no such path
	stack<int> pathTo(int v) {
		if (v == m_source) {
			stack<int> path;
			path.push(v);
			return path;
		}
		if (!hasPathTo(v) && v != m_source) 
			return std::stack<int>(); // return empty stack if no path exists
		stack<int> path;
		int i = v;
		do // reconstruct the path from the end 
		{
			path.push(i);			// put previous vertex on the stack
			i = m_edgeTo[i];		
		} while (i != m_source);
		path.push(m_source);		// put the source on the stack
		return path;
	}

	void printPathTo(int v) {
		if (!hasPathTo(v)) { 
			cout << "No path exists from " << m_source << " to " << v << endl;
		}
		else {
			stack<int> path = pathTo(v);
			cout << "Shortest path from " << m_source << " to " << v << " is ("
				<< distTo(v) << ") via ";
			cout << path.top();
			path.pop();
			while (!path.empty()) {
				cout << " -> " << path.top(); 
				path.pop();
			}
			cout << endl;
		}
		
	}
};

#endif
