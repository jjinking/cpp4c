#ifndef HEX_GAME_H
#define HEX_GAME_H

#include "graph.h"		// UndirectedGraph
#include "edge.h"		// Edge
#include "DijkstraSP.h"	// ShortestPath
#include <cstdlib>		// system()
#include <iostream>		// std::cout, std::cin
#include <conio.h>		// getchar
#include <string>		// std::string
#include <vector>		// std::vector

using namespace std;

/* Class representing the Hex board.
Nodes (Black and White) are stored in Undirected graphs.
Dijkstra's SP algorithm is used to determine the winning situation. */
class HexBoard {
private:
	int m_sideSize;							// Side length of the board
	shared_ptr<UndirectedGraph> m_BlackG;	// Graph representing Black nodes. Edges stand for connections between adjacent nodes.
	shared_ptr<UndirectedGraph> m_WhiteG;	// Graph representing White nodes. Edges stand for connections between adjacent nodes.

	enum NodeColor {	// color of the nodes 
		Black,		// 1st player
		White,		// 2nd player
		Empty		// untouched
	};


	vector<NodeColor> m_colorsVec;	// internal representation for nodes' color

	// transform (i,j) to N coordinates
	int ijToN(int i, int j) {
		return i * m_sideSize + j;
	}

	// compute first coordinate from N
	int iFromN(int n) {
		return n / m_sideSize;
	} 

	// compute second coordinate from N
	int jFromN(int n) {
		return n % m_sideSize;
	}

	// internal helper function - returns color name as string
	string colorName(NodeColor c) {
		if (c == Black)
			return "Black";
		else if (c == White)
			return "White";
		else
			return "Empty";
	}

public:
	HexBoard(int sideSize = 3) 
		: m_BlackG(shared_ptr<UndirectedGraph>(new UndirectedGraph(sideSize*sideSize))),	// initialise Black's Graph
		m_WhiteG(shared_ptr<UndirectedGraph>(new UndirectedGraph(sideSize*sideSize))),		// initialise White's Graph
		m_sideSize(sideSize),																// store side size internally
		m_colorsVec(vector<NodeColor>(sideSize*sideSize, Empty))							// initialise colors' array
	{
		if (sideSize < 3)  // less than 3x3 board makes no sense
			throw invalid_argument("Size of the board side must be at least 3.");
	}

	// try to set color. If fails due to non empty node, return false.
	bool setColor(int i, int j, NodeColor c) {
		int n = ijToN(i, j);
		if (m_colorsVec[n] == Empty)  // check if the node is empty
		{
			m_colorsVec[n] = c;  // set the color

			// select which board to update
			shared_ptr<UndirectedGraph> gPtr;			
			if (c == Black)
				gPtr = m_BlackG;
			else
				gPtr = m_WhiteG;


			// establish edges to the adjacent nodes of same :
			if (i != 0) {
				// node on the left
				if (m_colorsVec[ijToN(i-1, j)] == c) { 
					gPtr->addEdge(shared_ptr<Edge>(new Edge(ijToN(i,j), ijToN(i-1, j), 1)));
				}
			}
			if (i != m_sideSize-1) {
				// node on the right
				if (m_colorsVec[ijToN(i+1, j)] == c) {
					gPtr->addEdge(shared_ptr<Edge>(new Edge(ijToN(i,j), ijToN(i+1, j), 1)));
				}
			}
			if (j != 0) {
				// node up left
				if (m_colorsVec[ijToN(i, j-1)] == c) {
					gPtr->addEdge(shared_ptr<Edge>(new Edge(ijToN(i,j), ijToN(i, j-1), 1)));
				}
			}
			if (j != m_sideSize-1) {
				// node down right
				if (m_colorsVec[ijToN(i, j+1)] == c) {
					gPtr->addEdge(shared_ptr<Edge>(new Edge(ijToN(i,j), ijToN(i, j+1), 1)));
				}
			}
			if (i != 0 && j != m_sideSize-1) {
				// node up right
				if (m_colorsVec[ijToN(i-1, j+1)] == c) {
					gPtr->addEdge(shared_ptr<Edge>(new Edge(ijToN(i,j), ijToN(i-1, j+1), 1)));
				}
			}
			if (i != m_sideSize-1 && j != 0) {
				// node down left
				if (m_colorsVec[ijToN(i+1, j-1)] == c) {
					gPtr->addEdge(shared_ptr<Edge>(new Edge(ijToN(i,j), ijToN(i+1, j-1), 1)));
				}
			}

			return true;
		}
		else  // if the node is not empty
			return false;
	}

	// check if the board has a winning connection of any color
	NodeColor isConnected() {	

		// check if Black has a path from top to bottom
		for (int j = 0; j != m_sideSize; j++) {		// iterate over all Shortest Paths
			ShortestPath spBlack(*m_BlackG, ijToN(0, j));
			for (int k = 0; k != m_sideSize; k++) {
				// check if the shortest path connects two sides
				bool hasPath = spBlack.hasPathTo(ijToN(m_sideSize-1, k));
				if (hasPath)
					return Black;
			}			
		}

		// check if White has a path from left to right
		for (int i = 0; i != m_sideSize; i++) {
			ShortestPath spWhite(*m_WhiteG, ijToN(i, 0));
			for (int k = 0; k != m_sideSize; k++) {
				bool hasPath = spWhite.hasPathTo(ijToN(k , m_sideSize-1));
				if (hasPath)
					return White;
			}			
		}
		return Empty;
	}

	// print out the board in ASCII format
	void print() {
		int shift = 0;		
		for (int i = 0; i < m_sideSize-1; i++) {
			for (int j = 0; j < shift; j++) {
				cout << " ";
			}

			cout << m_colorsVec[ijToN(i, 0)];
			for (int j = 1; j < m_sideSize; j++) {
				cout << " - " << m_colorsVec[ijToN(i, j)];
			}
			cout << endl;
			shift++;

			for (int j = 0; j != shift; j++) {
				cout << " ";
			}
			shift++;

			cout << "\\";
			for (int j = 1; j < m_sideSize; j++) {
				cout << " / \\";
			}
			cout << endl;
		}

		for (int j = 0; j < shift; j++) {
			cout << " ";
		}

		cout << m_colorsVec[ijToN(m_sideSize-1, 0)];
		for (int j = 1; j < m_sideSize; j++) {
			cout << " - " << m_colorsVec[ijToN(m_sideSize-1, j)];
		}
		cout << endl;
	}

	friend ostream& operator<<(ostream& out, HexBoard::NodeColor& color);

	friend class HexGame;
};

// overloaded stream outout. allows a more simple board drawing.
ostream& operator<<(ostream& out, HexBoard::NodeColor& color) {

	if (color == HexBoard::Black)
		out << "x";
	else if (color == HexBoard::White)
		out << "o";
	else //(color == HexBoard::Color::Grey)
		out << ".";
	return out;
}

/* Class representing the game play of the Hex Game.*/
class HexGame{
private:
	shared_ptr<HexBoard> board;	// Hex board pointer.

	// enumeration of game player types
	enum PlayerType {
		Human,
		AI
	};

	// allows the player to meke a move via IO or lets the PC make it's move
	void makeMove(HexBoard::NodeColor c, PlayerType type)
	{
		// player makes his moves via input of the i'th and j'th coordinates
		if (type == Human) {
			int row, column;
			bool isNodeSelected = false;			
			while(!isNodeSelected) { // loop until a legal move is inserted
				cout << "Select a node!\nType in the row number from 1 to " << board->m_sideSize << ": ";
				cin >> row;
				cout << "Type in the column number from 1 to " << board->m_sideSize << ": ";
				cin >> column;
				isNodeSelected = board->setColor(row-1, column-1, c);  // set the color for the selected node
				if (!isNodeSelected)  // check if the move was legal
					cout << "Illegal move. The node is not empty." << endl;
			}
		}
		else if (type == AI) {
			throw invalid_argument("Not implemented. Part of the next Homework!");

		}		
		else
			throw invalid_argument("Not implemented");				
	}

public:
	// default constructor
	HexGame() {}

	// game play
	void start() {
		int n = 0;		// for user input. side length of the board
		int row = 0;	// for user input. row index.
		int column = 0; // for user input. column index.

		system("cls");  // clear screen
		cout << "You are starting a Hex game. Give the size of the board side: ";		
		cin >> n;		
		board.reset(new HexBoard(n));	// initialise the board

		HexBoard::NodeColor winColor = HexBoard::Empty;
		while(winColor == HexBoard::Empty) {  // loop (make moves) until the game is over
			// print the updated board
			system("cls");
			cout << "Black's turn." << endl;
			board->print();			// redraw the board

			// black makes the move
			makeMove(HexBoard::Black, Human);

			// Stop the game is Black wins
			winColor = board->isConnected();
			if (winColor == HexBoard::Black)
				break;

			// print the updated board
			system("cls");
			cout << "White's turn." << endl;
			board->print();	

			// white makes the move
			makeMove(HexBoard::White, Human);

			// Stop the game is White wins
			winColor = board->isConnected();
			if (winColor == HexBoard::White)
				break;
		}
		system("cls");		
		board->print();	
		cout << "Game Over. " << (board->colorName(winColor)) << " wins!" << endl;
		cout << "Press any key...";
		_getch();  // wait for a key to close the application

	}	
};
#endif

