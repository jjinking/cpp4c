#ifndef EDGE_H
#define EDGE_H

#include <exception>
#include <iostream>
#include <limits>

using namespace std;

class Edge { 
private:
    int m_v;		// one of the vertices
    int m_w;		// the other vertex
    double m_cost;	// weight of the edge
public:   
    // Constructor. Create an edge between v and w with given weight.
    Edge(int v, int w, double weight) : m_v(v), m_w(w), m_cost(weight) {}

    // Getter. Return the weight of this edge.    
    double weight() const { return m_cost; }

    // Getter. Return either endpoint of this edge.
	inline int either() const {
		return m_v;
	}

   /**
     * Return the endpoint of this edge that is different from the given vertex
     * (unless a self-loop).
     */
	inline int other(int vertex) const {
		if      (vertex == m_v) return m_w;
		else if (vertex == m_w) return m_v;
		else throw std::invalid_argument("Illegal endpoint");
	} 
   
    // Compare edges by weight.
	inline double compareTo(Edge that) const { 
		return m_cost - that.weight(); 
	}
};

// Send Edge to ostream
std::ostream& operator << (std::ostream& out, Edge& e) {
	out << e.either() << "-" << e.other(e.either()) << " (" << e.weight() << ")";
	return out;
}

#endif
