#ifndef GRAPH_H
#define GRAPH_H

#include <exception>    // std::exception
#include <fstream>		// std::fstream
#include <list>			// std::list
#include <random>		// std::mt19937, std::random_device, std::uniform_real_distribution
#include <vector>		// std::vector

using namespace std;

class UndirectedGraph {
private:
	int m_V;									// Number of vertices
	int m_E;									// Number of edges
	vector<list<shared_ptr<Edge> > > m_adjList;	// Adjacency list representation of the graph

public:	

	// Constructor: create an undirected graph with V vertices and 0 edges
	UndirectedGraph(const int V) : m_V(V), m_E(0) {
		if (V < 0) // check for incorrect input
			throw invalid_argument("number of vertices is negative");
		
		// create an empty adjacency list for m_V vertices
		m_adjList.reserve(m_V);
		for (int i = 0; i != m_V; i++) {
			list<shared_ptr<Edge> > edgeList;
			m_adjList.push_back(edgeList);
		}
	}

	/* Constructor: create a random undirected graph with V vertices
	   Number of edges is uniformly distributed with probability given by density input parameter
	   Edge length is uniformly distributed between leftBound and RightBond */
	UndirectedGraph(
		const int V, 
		const double density, 
		const double leftBound,
		const double rightBound) : m_V(V), m_E(0) {
		if (V < 0) // check for incorrect input
			throw invalid_argument("number of vertices is negative");

		// create an empty adjacency list for m_V vertices
		m_adjList.reserve(m_V);
		for (int i = 0; i != m_V; i++) {
			list<shared_ptr<Edge> > edgeList;
			m_adjList.push_back(edgeList);
		}

		// random number generator for probability
		mt19937 probRng;					
		probRng.seed(random_device()());
		uniform_real_distribution<double> probDist(0.0, 1.0);

		// random number generator for edge length
		mt19937 lengthRng;					
		lengthRng.seed(random_device()());
		uniform_real_distribution<double> lengthDist(leftBound, rightBound);

		double probability;

		for (int v = 0; v != m_V; v++) {
			for (int w = v+1; w != m_V; w++) {
				probability = probDist(probRng); // generate a random probability
				if (probability < density) {
					double cost = lengthDist(lengthRng); // generate a random edge length
					shared_ptr<Edge> e(new Edge(v, w, cost));
					addEdge(e);
				}
			}
		}
	}

	// Constructor: create an undirected graph from an input file
	UndirectedGraph(const string & fileName) {
		fstream in;
		in.open(fileName);			// read in file with graph representation

		in >> m_V; 
		if (m_V < 0) // check for incorrect input
			throw invalid_argument("number of vertices is negative");

		// create an empty adjacency list for m_V vertices
		m_adjList.reserve(m_V);
		for (int i = 0; i != m_V; i++) {
			list<shared_ptr<Edge> > edgeList;
			m_adjList.push_back(edgeList);
		}

		// read in the graph line by line
		int v, w;
		double d;
		while (!in.eof()) {
			in >> v;
			in >> w;
			in >> d;
			tr1::shared_ptr<Edge> e(new Edge(v, w, d));
			addEdge(e);
		}
	}

	// return the number of vertices in the graph
	inline int V() const {
		return m_V;
	}

	// return the number of edges in the graph
	inline int E() const {
		return m_E;
	}

	// add edge to the adjacency list
	void addEdge(shared_ptr<Edge> & edge) {
		int v = edge->either();
		int w = edge->other(v);
		if (v >= m_V || w >= m_V) 
			throw invalid_argument("Graph capacity exceeded");

		m_adjList[v].push_back(edge);		
		m_adjList[w].push_back(edge);
		m_E++;
	}

	// return all incident edges to the given vertex as a list
	inline list<shared_ptr<Edge> > adjEdges(int vertex) {
		return m_adjList[vertex];
	}

	// return all edges in the graph as a list
	list<shared_ptr<Edge> > allEdges() {
		list<shared_ptr<Edge> > edges;
		for (int v = 0; v != m_V; v++) {
			list<shared_ptr<Edge> > adjEdges_ = adjEdges(v);
			edges.insert(edges.end(), adjEdges_.begin(), adjEdges_.end());
		}
		return edges;
	}
};

ostream& operator<<(ostream& out, UndirectedGraph& G) {	
	list<shared_ptr<Edge> > edges;
	list<shared_ptr<Edge> >::iterator edgeIt;
	for (int v = 0; v != G.V(); v++) {
		edges = G.adjEdges(v);
		for (edgeIt = edges.begin(); edgeIt != edges.end(); edgeIt++) {			
			out << v << "-" << (*edgeIt)->other(v) << " (" << (*edgeIt)->weight() << ")" << " ; ";
		}
		out << endl;
	}
	return out;
}

#endif
