#include <exception>		// std::exception
#include <iostream>			// std::cout
#include "edge.h"			// Edge
#include "graph.h"			// UndirectedGraph
#include "HexGame.h"		// HexBoard, HexGame

using namespace std;


void runHexGame() {
	HexGame game;
	game.start();
}

int main(void)
{
	try {

		runHexGame();
	}
	catch (std::exception& e) {
		std::cerr << "exception cough: " << e.what() << std::endl;
	}

	return 0;
}

   