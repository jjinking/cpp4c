//
//  hexboard.cpp
//  hex
//
//  Created by Alexander Edward Drew on 11/15/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#include "hexboard.h"

HexBoard::HexBoard(std::size_t n)
{
    initializeMatrix(n);
}

void HexBoard::initializeMatrix(std::size_t n)
{
    matrix_.resize(n, std::vector<SpaceStatus>(n, SpaceStatus::EMPTY));
}


void HexBoard::SetSpaceStatus(std::size_t i, std::size_t j, SpaceStatus status)
{
    if (i < Size() && j < Size()) {
        matrix_[i][j] = status;
    }
}

HexBoard::SpaceStatus HexBoard::GetSpaceStatus(std::size_t i, std::size_t j) const
{
    SpaceStatus status = SpaceStatus::EMPTY;
    if (i < Size() && j < Size()) {
        status = matrix_[i][j];
    }
    return status;
}

std::size_t HexBoard::Size() const
{
    return matrix_.size();
}



std::ostream& operator<<(std::ostream& os, const HexBoard& hexboard)
{
    for (std::size_t i = 0; i < hexboard.matrix_.size(); ++i) {
        // Insert 2*i spaces
        for (std::size_t j = 0; j < 2 * i; ++j) {
            os << " ";
        }
        auto row = hexboard.matrix_[i];
        for (std::size_t j = 0; j < row.size(); ++j) {
            auto space = row[j];
            switch (space) {
                case HexBoard::SpaceStatus::BLUE:
                    os << "B";
                    break;
                case HexBoard::SpaceStatus::RED:
                    os << "R";
                    break;
                default:    // EMPTY
                    os << ".";
                    break;
            }
            // Insert line unless last space
            if (j != row.size() - 1) {
                os << " - ";
            }
        }
        os << std::endl;
        // Insert 2*i + 1 spaces
        for (std::size_t j = 0; j < 2*i + 1; ++j) {
            os << " ";
        }
        // Inseart semi-vertical paths unless last row
        if (i != hexboard.matrix_.size() - 1) {
            for (std::size_t j = 0; j < 2 * row.size() - 1; ++j) {
                if (j % 2 == 0) {
                    os << "\\";
                } else {
                    os << " / ";
                }
            }
        }
        os << std::endl;
        
    }
    return os;
}
