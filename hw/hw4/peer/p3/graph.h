//
//  Graph.h
//  djikstra
//
//  Created by Alexander Edward Drew on 10/26/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#ifndef __hex__graph__
#define __hex__graph__

#include <iostream>
#include <list>
#include <map>
#include <string>
#include <utility>

typedef size_t Vertex;

typedef std::list<Vertex> VertexList;

struct Edge
{
    Vertex vertex0;
    Vertex vertex1;
    double weight;
};

typedef std::list<Edge> EdgeList;

class Graph
{
public:
    Graph(size_t num_vertices);
    Graph(std::string edge_file);
    size_t num_vertices() const;
    size_t num_edges() const;
    void insert_edge(const Vertex& u,
                     const Vertex& v,
                     double distance);
    void insert_vertex(const Vertex& u);
    EdgeList edges_of(const Vertex& u) const;
    void remove_edge(const Edge& edge);
    void remove_vertex(const Vertex& v);
    bool contains(const Vertex& v) const;
    VertexList vertices() const;
    bool is_path(const Vertex& u,
                 const Vertex& v);
private:
    void add_vertices(size_t num_vertices);
    // Data
    std::map<Vertex, EdgeList> edge_lists_;
    size_t num_edges_;
    
    friend std::ostream& operator<<(std::ostream& os, const Graph& g);
};

std::ostream& operator<<(std::ostream& os, const Graph& g);

#endif /* defined(__hex__graph__) */
