//
//  humanhexplayer.cpp
//  hex
//
//  Created by Alexander Edward Drew on 11/16/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#include "humanhexplayer.h"

std::pair<std::size_t, std::size_t> HumanHexPlayer::GetMove(const HexBoard& board)
{
    std::cout << "Please enter your move in the form of two non-negatice integers (e.g. `2 3`)" << std::endl;
    std::size_t i(0), j(0);
    std::cin >> i;
    std::cin >> j;
    return std::make_pair(i, j);
}