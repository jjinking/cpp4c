//
//  hexrules.h
//  hex
//
//  Created by Alexander Edward Drew on 11/16/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#ifndef __hex__hexrules__
#define __hex__hexrules__

#include <iostream>
#include <utility>

#include "graph.h"
#include "hexboard.h"

class HexRules
{
public:
    enum class PlayerColor : short {
        RED = 0,
        BLUE
    };
    bool IsLegalMove(std::size_t i, std::size_t j, const HexBoard& board) const;
    bool DidPlayerWin(PlayerColor player, const HexBoard& board) const;
private:
    std::size_t uniqueSpaceID(std::size_t i, std::size_t j, const HexBoard& board) const;
    std::pair<std::size_t, std::size_t> coords(std::size_t id, const HexBoard& board) const;
    const Graph generateGraph(PlayerColor color, const HexBoard& board) const;
    std::size_t source(const HexBoard& board) const;
    std::size_t sink(const HexBoard& board) const;
};

#endif /* defined(__hex__hexrules__) */
