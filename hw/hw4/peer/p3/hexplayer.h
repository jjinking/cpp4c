//
//  hexplayer.h
//  hex
//
//  Created by Alexander Edward Drew on 11/16/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#ifndef __hex__hexplayer__
#define __hex__hexplayer__

#include <iostream>
#include <utility>

class HexBoard;

class HexPlayer {
public:
    virtual std::pair<std::size_t, std::size_t> GetMove(const HexBoard& board) = 0;
    virtual ~HexPlayer() {};
};

#endif /* defined(__hex__hexplayer__) */
