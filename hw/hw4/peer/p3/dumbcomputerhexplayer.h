//
//  dumbcomputerhexplayer.h
//  hex
//
//  Created by Alexander Edward Drew on 11/16/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#ifndef __hex__dumbcomputerhexplayer__
#define __hex__dumbcomputerhexplayer__

#include <iostream>
#include <random>

#include "hexplayer.h"

class DumbComputerHexPlayer : public HexPlayer
{
public:
    virtual std::pair<std::size_t, std::size_t> GetMove(const HexBoard& board);
private:
    std::default_random_engine generator_;
};

#endif /* defined(__hex__dumbcomputerhexplayer__) */
