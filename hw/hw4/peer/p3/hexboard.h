//
//  hexboard.h
//  hex
//
//  Created by Alexander Edward Drew on 11/15/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#ifndef __hex__hexboard__
#define __hex__hexboard__

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

// A class for modeling a hex board of arbitrary size.
class HexBoard {
public:
    // Types
    enum class SpaceStatus : short {
        EMPTY = 0,
        RED,
        BLUE
    };
    using SpaceStatusMatrix = std::vector<std::vector<SpaceStatus> >;
    // Constructors
    explicit HexBoard(std::size_t n = 7);
    // Member functions
    void SetSpaceStatus(std::size_t i, std::size_t j, SpaceStatus status);
    SpaceStatus GetSpaceStatus(std::size_t i, std::size_t j) const;
    std::size_t Size() const;
protected:
private:
    void initializeMatrix(std::size_t n);
    // Store the board as a
    // two-dimensional matrix
    // of SpaceStatus
    SpaceStatusMatrix matrix_;
    
    friend std::ostream& operator<<(std::ostream& os, const HexBoard& hexboard);
};

std::ostream& operator<<(std::ostream& os, const HexBoard& hexboard);

#endif /* defined(__hex__hexboard__) */
