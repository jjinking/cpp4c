//
//  dumbcomputerhexplayer.cpp
//  hex
//
//  Created by Alexander Edward Drew on 11/16/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#include "dumbcomputerhexplayer.h"

#include "hexboard.h"

std::pair<std::size_t, std::size_t> DumbComputerHexPlayer::GetMove(const HexBoard& board)
{
    std::uniform_int_distribution<std::size_t> distribution(0,board.Size() - 1);
    std::size_t i = distribution(generator_);
    std::size_t j = distribution(generator_);
    return std::make_pair(i, j);
}