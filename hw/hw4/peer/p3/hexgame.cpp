//
//  hexgame.cpp
//  hex
//
//  Created by Alexander Edward Drew on 11/16/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#include "hexgame.h"

HexGame::HexGame()
: board_(nullptr), rules_(nullptr), blue_player_(nullptr), red_player_(nullptr)
{}

HexGame::~HexGame()
{
    delete board_;
    delete rules_;
    delete blue_player_;
    delete red_player_;
}

void HexGame::Start()
{
    initialize();
    printBoard();
    play();
}

void HexGame::initialize()
{
    std::cout << "Please choose the board size" << std::endl;
    std::size_t board_size(0);
    std::cin >> board_size;
    board_ = new HexBoard(board_size);
    rules_ = new HexRules();
    std::cout << "Would you like to play as blue? (y/n)" << std::endl;
    char response;
    std::cin >> response;
    if (response == 'y' || response == 'Y' || response == '1') {
        blue_player_ = new HumanHexPlayer;
        red_player_ = new DumbComputerHexPlayer;
    } else {
        blue_player_ = new DumbComputerHexPlayer;
        red_player_ = new HumanHexPlayer;
    }
}

void HexGame::play()
{
    while (true) {
        std::cout << "Blue turn" << std::endl;
        std::pair<std::size_t, std::size_t> blue_move = blue_player_->GetMove(*board_);
        while (!(rules_->IsLegalMove(blue_move.first, blue_move.second, *board_))) {
            blue_move = blue_player_->GetMove(*board_);
        }
        board_->SetSpaceStatus(blue_move.first, blue_move.second, HexBoard::SpaceStatus::BLUE);
        printBoard();
        if (rules_->DidPlayerWin(HexRules::PlayerColor::BLUE, *board_)) {
            std::cout << "Blue player has won!!!" << std::endl;
            return;
        }
        std::cout << "Red turn" << std::endl;
        std::pair<std::size_t, std::size_t> red_move = red_player_->GetMove(*board_);
        while (!(rules_->IsLegalMove(red_move.first, red_move.second, *board_))) {
            red_move = red_player_->GetMove(*board_);
        }
        board_->SetSpaceStatus(red_move.first, red_move.second, HexBoard::SpaceStatus::RED);
        printBoard();
        if (rules_->DidPlayerWin(HexRules::PlayerColor::RED, *board_)) {
            std::cout << "Red player has won!!!" << std::endl;
            return;
        }
    }
}

void HexGame::printBoard() const
{
    std::cout << *board_;
}