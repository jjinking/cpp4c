//
//  humanhexplayer.h
//  hex
//
//  Created by Alexander Edward Drew on 11/16/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#ifndef __hex__humanhexplayer__
#define __hex__humanhexplayer__

#include <iostream>
#include "hexplayer.h"

class HumanHexPlayer : public HexPlayer
{
public:
    virtual std::pair<std::size_t, std::size_t> GetMove(const HexBoard& board);
};

#endif /* defined(__hex__humanhexplayer__) */
