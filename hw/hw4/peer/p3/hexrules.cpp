//
//  hexrules.cpp
//  hex
//
//  Created by Alexander Edward Drew on 11/16/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#include "hexrules.h"

bool HexRules::IsLegalMove(std::size_t i, std::size_t j, const HexBoard& board) const
{
    bool is_legal = false;
    // Legal if on the board and empty
    if (i < board.Size() &&
        j < board.Size() &&
        board.GetSpaceStatus(i, j) == HexBoard::SpaceStatus::EMPTY) {
        is_legal = true;
    }
    return is_legal;
}

bool HexRules::DidPlayerWin(PlayerColor player, const HexBoard& board) const
{
    bool did_player_win = false;
    Graph g = generateGraph(player, board);
    did_player_win = g.is_path(source(board), sink(board));
    return did_player_win;
}

std::size_t HexRules::uniqueSpaceID(std::size_t i, std::size_t j, const HexBoard& board) const
{
    return board.Size() * i + j;
}

std::pair<std::size_t, std::size_t> HexRules::coords(std::size_t id, const HexBoard& board) const
{
    return std::make_pair(id / board.Size(), id % board.Size());
}

const Graph HexRules::generateGraph(PlayerColor color, const HexBoard& board) const
{
    Graph g(0); // graph with zero vertices
    // Define the source
    // in order to leave the ids for the normal positions untouched
    g.insert_vertex(source(board));
    g.insert_vertex(sink(board));
    if (color == PlayerColor::RED) {
        // Proceed to build graph from source to sink,
        // top to bottom
        // for all rows, look for one column to left, one row above, and
        // one column right and one row above
        // Row-first ordering
        for (std::size_t i = 0; i < board.Size(); ++i) {
            for (std::size_t j = 0; j < board.Size(); ++j) {
                if (board.GetSpaceStatus(i, j) == HexBoard::SpaceStatus::RED) {
                    std::size_t id1 = uniqueSpaceID(i, j, board);
                    g.insert_vertex(id1);
                    if (board.GetSpaceStatus(i - 1, j) == HexBoard::SpaceStatus::RED) {
                        std::size_t id0 = uniqueSpaceID(i - 1, j, board);
                        g.insert_edge(id0, id1, 1.0);
                    }
                    if (board.GetSpaceStatus(i - 1, j + 1) == HexBoard::SpaceStatus::RED) {
                        std::size_t id0 = uniqueSpaceID(i - 1, j + 1, board);
                        g.insert_edge(id0, id1, 1.0);
                    }
                    if (board.GetSpaceStatus(i, j - 1) == HexBoard::SpaceStatus::RED) {
                        std::size_t id0 = uniqueSpaceID(i, j - 1, board);
                        g.insert_edge(id0, id1, 1.0);
                    }
                }
            }
        }
        // treat row 0 specially: all have an edge to the source
        for (std::size_t j = 0; j < board.Size(); ++j) {
            if (board.GetSpaceStatus(0, j) == HexBoard::SpaceStatus::RED) {
                std::size_t id = uniqueSpaceID(0, j, board);
                g.insert_edge(source(board), id, 1.0);
            }
        }
        // treat the last row (size - 1) specially: all have edges to the sink
        for (std::size_t j = 0; j < board.Size(); ++j) {
            if (board.GetSpaceStatus(board.Size() - 1, j) == HexBoard::SpaceStatus::RED) {
                std::size_t id = uniqueSpaceID(board.Size() - 1, j, board);
                g.insert_edge(id, sink(board), 1.0);
            }
        }
    } else {    // BLUE
        // Proceed to build graph from source to sink,
        // left to right
        // for all rows, look for one column to left, one row above, and
        // one column right and one row above
        // Row-first ordering
        for (std::size_t i = 0; i < board.Size(); ++i) {
            for (std::size_t j = 0; j < board.Size(); ++j) {
                if (board.GetSpaceStatus(i, j) == HexBoard::SpaceStatus::BLUE) {
                    std::size_t id1 = uniqueSpaceID(i, j, board);
                    g.insert_vertex(id1);
                    if (board.GetSpaceStatus(i - 1, j) == HexBoard::SpaceStatus::BLUE) {
                        std::size_t id0 = uniqueSpaceID(i - 1, j, board);
                        g.insert_edge(id0, id1, 1.0);
                    }
                    if (board.GetSpaceStatus(i - 1, j + 1) == HexBoard::SpaceStatus::BLUE) {
                        std::size_t id0 = uniqueSpaceID(i - 1, j + 1, board);
                        g.insert_edge(id0, id1, 1.0);
                    }
                    if (board.GetSpaceStatus(i, j - 1) == HexBoard::SpaceStatus::BLUE) {
                        std::size_t id0 = uniqueSpaceID(i, j - 1, board);
                        g.insert_edge(id0, id1, 1.0);
                    }
                }
            }
        }
        // treat col 0 specially: all have an edge to the source
        for (std::size_t i = 0; i < board.Size(); ++i) {
            if (board.GetSpaceStatus(i, 0) == HexBoard::SpaceStatus::BLUE) {
                std::size_t id = uniqueSpaceID(i, 0, board);
                g.insert_edge(source(board), id, 1.0);
            }
        }
        // treat the last col (size - 1) specially: all have edges to the sink
        for (std::size_t i = 0; i < board.Size(); ++i) {
            if (board.GetSpaceStatus(i, board.Size() - 1) == HexBoard::SpaceStatus::BLUE) {
                std::size_t id = uniqueSpaceID(i, board.Size() - 1, board);
                g.insert_edge(id, sink(board), 1.0);
            }
        }
    }
    return g;
}

std::size_t HexRules::source(const HexBoard& board) const
{
    return board.Size() * board.Size();
}

std::size_t HexRules::sink(const HexBoard& board) const
{
    return board.Size() * board.Size() + 1;
}
