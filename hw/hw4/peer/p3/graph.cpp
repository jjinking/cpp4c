//
//  Graph.cpp
//  djikstra
//
//  Created by Alexander Edward Drew on 10/26/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#include "graph.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <stack>
#include <unordered_map>

Graph::Graph(size_t num_vertices) :
num_edges_(0)
{
    add_vertices(num_vertices);
}

Graph::Graph(std::string edge_file) :
num_edges_(0)
{
    std::ifstream in_file(edge_file, std::ifstream::in);
    if (in_file.fail()) {
        std::cout << "Failed to open file: " << edge_file << std::endl;
        return;
    }
    size_t num_vertices(0);
    std::string line;
    std::getline(in_file, line);
    std::stringstream line_stream(line);
    line_stream >> num_vertices;
    add_vertices(num_vertices);
    while (std::getline(in_file, line)) {
        std::stringstream line_stream(line);
        size_t v0(0);
        size_t v1(0);
        double weight(0.0);
        line_stream >> v0;
        line_stream >> v1;
        line_stream >> weight;
        // Dont double count listing in opposite order
        if (v0 < v1) {
            insert_edge(v0, v1, weight);
        }
    }
}

size_t Graph::num_vertices() const
{
    return edge_lists_.size();
}

void Graph::insert_edge(const Vertex& u,
                        const Vertex& v,
                        double distance)
{
    edge_lists_[u].push_back({u, v, distance});
    edge_lists_[v].push_back({v, u, distance});
    ++num_edges_;
}

void Graph::insert_vertex(const Vertex& u)
{
    edge_lists_[u] = EdgeList(); // start with empty list
}

EdgeList Graph::edges_of(const Vertex& u) const
{
    auto it = edge_lists_.find(u);
    if (it != edge_lists_.end()) {
        return edge_lists_.at(u);
    }
    return EdgeList();
}

void Graph::remove_edge(const Edge& edge)
{
    Vertex v0 = edge.vertex0;
    Vertex v1 = edge.vertex1;
    edge_lists_[v0].remove_if([v1](Edge e) { return e.vertex1 == v1; });
    edge_lists_[v1].remove_if([v0](Edge e) { return e.vertex1 == v0; });
    --num_edges_;
}

void Graph::remove_vertex(const Vertex& v)
{
    std::map<Vertex, EdgeList>::iterator it = edge_lists_.find(v);
    if (it != edge_lists_.end()) {
        edge_lists_.erase(it);
    }
}

bool Graph::contains(const Vertex& v) const
{
    return edge_lists_.count(v) != 0;
}

VertexList Graph::vertices() const
{
    VertexList verts;
    for (std::map<Vertex, EdgeList>::const_iterator it = edge_lists_.cbegin();
         it != edge_lists_.cend();
         ++it) {
        verts.push_back(it->first);
    }
    return verts;
}

void Graph::add_vertices(size_t num_vertices)
{
    for (Vertex u = 0; u < num_vertices; ++u) {
        insert_vertex(u);
    }
}

size_t Graph::num_edges() const
{
    return num_edges_;
}

bool Graph::is_path(const Vertex& u,
                    const Vertex& v)
{
    if (u == v) {
        return true;
    }
    // Use a stack-based DFS
    std::stack<Edge> edge_stack;
    // Use a map to keep track of
    // visited vertices
    std::unordered_map<Vertex, bool> visited_map;
    for (auto pair : edge_lists_) {
        Vertex w = pair.first;
        visited_map[w] = false;
    }
    // Initialize with vertex u and its edges
    visited_map[u] = true;
    EdgeList edges = edges_of(u);
    for (auto edge : edges) {
        edge_stack.push(edge);
    }
    while (!edge_stack.empty()) {
        Edge e = edge_stack.top();
        edge_stack.pop();
        Vertex w = e.vertex1;
        if (w == v) {
            return true;
        } else if (!visited_map[w]) {
            visited_map[w] = true;
            EdgeList edges = edges_of(w);
            for (auto edge : edges) {
                edge_stack.push(edge);
            }
        }
        // If already visited, do nothing
    }
    return false;
}

std::ostream& operator<<(std::ostream& os, const Graph& g)
{
    os << g.num_vertices() << " vertices" << std::endl;
    for (auto pair : g.edge_lists_) {
        os << pair.first << std::endl;
    }
    os << g.num_edges() << " undirected edges" << std::endl;
    for (auto pair : g.edge_lists_) {
        for (auto edge : pair.second) {
            os << edge.vertex0 << "--->" << edge.vertex1 << std::endl;
        }
    }
    os << std::endl;
    return os;
}