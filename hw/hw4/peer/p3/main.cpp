//
//  main.cpp
//  hex
//
//  Created by Alexander Edward Drew on 11/15/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#include <iostream>

#include "hexgame.h"

int main(int argc, const char * argv[])
{
    HexGame game;
    game.Start();
    
    return 0;
}

