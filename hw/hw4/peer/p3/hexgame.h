//
//  hexgame.h
//  hex
//
//  Created by Alexander Edward Drew on 11/16/13.
//  Copyright (c) 2013 Alexander Edward Drew. All rights reserved.
//

#ifndef __hex__hexgame__
#define __hex__hexgame__

#include <iostream>

#include "hexboard.h"
#include "hexrules.h"
#include "humanhexplayer.h"
#include "dumbcomputerhexplayer.h"

class HexGame
{
public:
    HexGame();
    ~HexGame();
    
    void Start();
    
private:
    void initialize();
    void play();
    void printBoard() const;
    
    HexBoard * board_;
    HexRules * rules_;
    HexPlayer * blue_player_;
    HexPlayer * red_player_;
};

#endif /* defined(__hex__hexgame__) */
