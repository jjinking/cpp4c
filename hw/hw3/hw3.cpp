// HW 3
// Implement minimum spanning tree algorithm
// Graph class is implemented as adjacency matrix to keep track of adjacent nodes
//   A 2-dimensional std::map is used as a sparse matrix for the adjacency matrix
// NB: I tried to follow Google's coding style guidelines
//   i.e. class member variables are postfixed by an underscore
//   (http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml)

//#include <ctime>
#include <fstream>
#include <iostream>
#include "undirectedgraph.h"

using namespace std;

void PrintVector(const vector<int>& v) {
  for (vector<int>::const_iterator i = v.begin(); i != v.end(); ++i)
    cout << *i << " ";
}

void TestGraph1() {
  // Test initialization and number of nodes and edges
  cout << "Initializing graph with 5 nodes" << endl;
  UndirectedGraph g = UndirectedGraph(5);
  cout << "n nodes: " << g.V() << endl;
  cout << "n edges: " << g.E() << endl;
  cout << endl;

  // Test adding edges
  cout << "Testing AddEdge(x,y)" << endl;
  cout << "Adding and edge between nodes 2 and 3" << endl;
  g.AddEdge(2,3);
  cout << "Adding edge between nodes 0 and 4" << endl;
  g.AddEdge(0,4);
  cout << "n nodes: " << g.V() << endl;
  cout << "n edges: " << g.E() << endl;
  cout << endl;
  cout << "Adding edge between nodes 1 and 2" << endl;
  g.AddEdge(1,2);
  cout << "Adding edge between nodes 2 and 4" << endl;
  g.AddEdge(2,4);
  cout << "n nodes: " << g.V() << endl;
  cout << "n edges: " << g.E() << endl;
  cout << endl;

  // Test IsAdjacent
  cout << "Testing IsAdjacent()" << endl;
  cout << "Nodes 2 and 3 are adjacent: " << g.IsAdjacent(2,3) << endl;
  cout << "Nodes 1 and 4 are adjacent: " << g.IsAdjacent(1,4) << endl;
  cout << "Nodes 2 and 4 are adjacent: " << g.IsAdjacent(2,4) << endl;
  cout << "Nodes 4 and 2 are adjacent: " << g.IsAdjacent(4,2) << endl;
  cout << "Nodes 2 and 1 are adjacent: " << g.IsAdjacent(2,1) << endl;
  cout << endl;
  
  // Test Neighbors
  cout << "Testing Neighbors()" << endl;
  cout << "Neighbors of 0: ";
  PrintVector(g.Neighbors(0));
  cout << endl;
  cout << "Neighbors of 4: ";
  PrintVector(g.Neighbors(4));
  cout << endl;
  cout << "Neighbors of 2: ";
  PrintVector(g.Neighbors(2));
  cout << endl;
  cout << "Neighbors of 1: ";
  PrintVector(g.Neighbors(1));
  cout << endl << endl;
  
  // Test DeleteEdge
  cout << "Testing DeleteEdge()" << endl;
  cout << "Deleting edge (4,2)" << endl;
  g.DeleteEdge(4,2);
  cout << "n nodes: " << g.V() << endl;
  cout << "n edges: " << g.E() << endl;
  cout << "Neighbors of 2: ";
  PrintVector(g.Neighbors(2));
  cout << endl;
  cout << "Neighbors of 0: ";
  PrintVector(g.Neighbors(0));
  cout << endl;
  cout << "IsAdjacent(2,4): " << g.IsAdjacent(2,4) << endl;
  cout << endl;
  cout << "Deleting edge (2,3)" << endl;
  g.DeleteEdge(2,3);
  cout << "n nodes: " << g.V() << endl;
  cout << "n edges: " << g.E() << endl;
  cout << "Neighbors of 2: ";
  PrintVector(g.Neighbors(2));
  cout << endl;
  cout << "Neighbors of 3: ";
  PrintVector(g.Neighbors(3));
  cout << endl;
  cout << "IsAdjacent(3,2): " << g.IsAdjacent(2,4) << endl;
  cout << endl;

  // Re-add edges (2,4) and (2,3)
  cout << "Re-adding edges (2,4) and (2,3)" << endl << endl;;
  g.AddEdge(2,4);
  g.AddEdge(2,3);

  // Test GetNodeValue and SetNodeValue
  cout << "Testing GetNodeValue and SetNodeValue" << endl;
  cout << "Value of node 0: " << g.GetNodeValue(0) << endl;
  cout << "Setting value of node 0 to 100 " << endl;
  g.SetNodeValue(0,100);
  cout << "Value of node 0: " << g.GetNodeValue(0) << endl;
  
  cout << "Value of node 2: " << g.GetNodeValue(2) << endl;
  cout << "Setting value of node 2 to 200 " << endl;
  g.SetNodeValue(2,200);
  cout << "Value of node 2: " << g.GetNodeValue(2) << endl;
  cout << endl;

  // Test GetEdgeValue SetEdgeValue
  cout << "Testing GetEdgeValue and SetEdgeValue" << endl;
  cout << "Value of edge (2,4): " << g.GetEdgeValue(2,4) << endl;
  cout << "Setting value of edge (2,4) to 4.5" << endl;
  g.SetEdgeValue(2,4,4.5);
  cout << "Value of edge (2,4): " << g.GetEdgeValue(2,4) << endl;
  cout << "Value of edge (2,3): " << g.GetEdgeValue(2,3) << endl;
  cout << "Setting value of edge (2,3) to 7.68" << endl;
  g.SetEdgeValue(2,3,7.68);
  cout << "Value of edge (2,3): " << g.GetEdgeValue(2,3) << endl;
}

void TestGraph2() {
  ifstream graphfile("examplegraph.2.txt");
  if (graphfile.is_open()) {
    UndirectedGraph g = UndirectedGraph(graphfile);
    cout << "Num nodes: " << g.V() << endl;
    cout << "Num edges: " << g.E() << endl;
    graphfile.close();
  }
  
  UndirectedGraph(g) = UndirectedGraph(5);
  cout << "Num nodes g2: " << g.V() << endl;
  cout << "Num edges g2: " << g.E() << endl;
  g.AddEdge(2,4,0);
  cout << "Num edges g2: " << g.E() << endl;
}

int main() {
  // Read in graph data
  ifstream graphfile("examplegraph.txt");
  if (graphfile.is_open()) {
    UndirectedGraph g = UndirectedGraph(graphfile);
    graphfile.close();
    // Print MST cost
    UndirectedGraph *mst = g.PrimMST();
    cout << "Done computing MST" << endl;
    cout << "MST Cost: " << mst->TotalEdgeCost() << endl;
    // Print MST edges
    cout << "MST Edges:" << endl;
    for (int i = 0; i < mst->V(); ++i)
      for (int j = 0; j < i; ++j)
	if (mst->IsAdjacent(i,j))
	  cout << "\t(" << i << "," << j << ")" << endl;
  }
}
